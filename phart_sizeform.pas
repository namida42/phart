unit phart_sizeform;

// Copyright (c) 2020 Namida Verasche
// Phart source code is made available under the MIT Licence
// See readme.txt for more information

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, Spin, StdCtrls,
  ExtCtrls;

type

  { TFImageSize }

  TFImageSize = class(TForm)
    btnOK: TButton;
    btnCancel: TButton;
    lbImageSize: TLabel;
    lbWhyDoesThisNeedAName: TLabel;
    rgAnchor: TRadioGroup;
    seWidth: TSpinEdit;
    seHeight: TSpinEdit;
  private
    function GetWidthValue: Integer;
    function GetHeightValue: Integer;
    function GetAnchorValue: Integer;
  public
    function ShowNew: Integer;
    function ShowResize(aWidth, aHeight: Integer): Integer;

    property WidthValue: Integer read GetWidthValue;
    property HeightValue: Integer read GetHeightValue;
    property AnchorValue: Integer read GetAnchorValue;
  end;

var
  FImageSize: TFImageSize;

implementation

{$R *.lfm}

{ TFImageSize }

function TFImageSize.GetWidthValue: Integer;
begin
  Result := seWidth.Value;
end;

function TFImageSize.GetHeightValue: Integer;
begin
  Result := seHeight.Value;
end;

function TFImageSize.GetAnchorValue: Integer;
begin
  Result := rgAnchor.ItemIndex;
end;

function TFImageSize.ShowNew: Integer;
begin
  seWidth.Value := 16;
  seHeight.Value := 16;
  rgAnchor.Visible := false;
  rgAnchor.Enabled := false;
  btnOK.Top := 48 * Screen.PixelsPerInch div 96;
  btnCancel.Top := 48 * Screen.PixelsPerInch div 96;
  ClientHeight := 97 * Screen.PixelsPerInch div 96;
  Result := ShowModal;
end;

function TFImageSize.ShowResize(aWidth, aHeight: Integer): Integer;
begin
  seWidth.Value := aWidth;
  seHeight.Value := aHeight;
  rgAnchor.Visible := true;
  rgAnchor.Enabled := true;
  btnOK.Top := 168 * Screen.PixelsPerInch div 96;
  btnCancel.Top := 168 * Screen.PixelsPerInch div 96;
  ClientHeight := 217 * Screen.PixelsPerInch div 96;
  Result := ShowModal;
end;

end.

