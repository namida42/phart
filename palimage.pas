unit palimage;

// Copyright (c) 2020 Namida Verasche
// Phart source code is made available under the MIT Licence
// See readme.txt for more information

{$mode objfpc}{$H+}
{$WARN 4080 off : Converting the operands to "$1" before doing the subtract could prevent overflow errors.}
interface

uses
  Math,
  GR32, GR32_OrdinalMaps,
  Classes, SysUtils;

type

  { TPalImage }

  TPreserveAnchor = (paClear, paTopLeft, paTop, paTopRight, paLeft, paCenter,
                     paRight, paBottomLeft, paBottom, paBottomRight);

  TPalImageChangeProc = procedure of object;

  TPalImage = class
    private
      fPixelData: TByteMap;
      fPalette: array[0..127] of TColor32;
      fOnChange: TPalImageChangeProc;

      function GetPixelIndex(X, Y: Integer): Byte;
      procedure SetPixelIndex(X, Y: Integer; aValue: Byte);
      function GetPixelColor(X, Y: Integer): TColor32;
      procedure SetPixelColor(X, Y: Integer; aValue: TColor32);
      function GetPaletteEntry(aIndex: Byte): TColor32;
      procedure SetPaletteEntry(aIndex: Byte; aValue: TColor32);

      function GetEquivalentPaletteIndex(aColor: TColor32): Byte;
      procedure SetWidth(aValue: Integer);
      procedure SetHeight(aValue: Integer);
      function GetWidth: Integer;
      function GetHeight: Integer;

      procedure Changed;
    public
      constructor Create;
      destructor Destroy; override;

      procedure Assign(aSrc: TPalImage);

      procedure LoadFromFile(aFile: String);
      procedure SaveToFile(aFile: String);
      procedure LoadFromStream(aStream: TStream);
      procedure SaveToStream(aStream: TStream);    

      procedure SetSize(aWidth, aHeight: Integer; aAnchor: TPreserveAnchor = paClear; aSpaceColor: Byte = 0);
      procedure Clear(aIndex: Byte = 0);
      procedure ClearColor(aColor: TColor32 = $00000000);

      procedure LoadPaletteFromFile(aFile: String; aTextBased: Boolean);
      procedure SavePaletteToFile(aFile: String; aTextBased: Boolean);
      procedure LoadPaletteFromStream(aStream: TStream; aTextBased: Boolean);
      procedure SavePaletteToStream(aStream: TStream; aTextBased: Boolean);
      procedure LoadPaletteFromImage(aSource: TPalImage);
      procedure SetPaletteToDefault;

      procedure WriteBitmap32(Dst: TBitmap32); overload;
      procedure WriteBitmap32(Dst: TBitmap32; aOrigin: TPoint); overload;

      procedure ReadBitmap32(Src: TBitmap32); overload;
      procedure ReadBitmap32(Src: TBitmap32; aRegion: TRect); overload;

      property Pixel[X, Y: Integer]: Byte read GetPixelIndex write SetPixelIndex; default;
      property PixelColor[X, Y: Integer]: TColor32 read GetPixelColor write SetPixelColor;
      property Palette[Index: Byte]: TColor32 read GetPaletteEntry write SetPaletteEntry;

      property Width: Integer read GetWidth write SetWidth;
      property Height: Integer read GetHeight write SetHeight;

      property OnChange: TPalImageChangeProc read fOnChange write fOnChange;
  end;

implementation

{ TPalImage }    

constructor TPalImage.Create;
begin
  fPixelData := TByteMap.Create;
end;

destructor TPalImage.Destroy;
begin
  fPixelData.Free;
  inherited Destroy;
end;

procedure TPalImage.Assign(aSrc: TPalImage);
begin
  fPixelData.Assign(aSrc.fPixelData);
  fPalette := aSrc.fPalette;
  fOnChange := aSrc.fOnChange;
end;

function TPalImage.GetPixelIndex(X, Y: Integer): Byte;
begin
  Result := fPixelData[X, Y];
end;

procedure TPalImage.SetPixelIndex(X, Y: Integer; aValue: Byte);
begin
  if fPixelData[X, Y] = aValue then Exit;
  fPixelData[X, Y] := aValue;
  Changed;
end;

function TPalImage.GetPixelColor(X, Y: Integer): TColor32;
begin
  Result := fPalette[fPixelData[X, Y]];
end;

procedure TPalImage.SetPixelColor(X, Y: Integer; aValue: TColor32);
begin
  fPixelData[X, Y] := GetEquivalentPaletteIndex(aValue);
  Changed;
end;

function TPalImage.GetPaletteEntry(aIndex: Byte): TColor32;
begin
  Result := fPalette[aIndex];
end;

procedure TPalImage.SetPaletteEntry(aIndex: Byte; aValue: TColor32);
begin
  fPalette[aIndex] := aValue;
  Changed;
end;

function TPalImage.GetEquivalentPaletteIndex(aColor: TColor32): Byte;
var
  DiffTable: array[0..127] of Int64;
  AlphaDiffTable: array[0..127] of Int64;
  HasNullAlpha: Boolean;
  MinDiff: Int64;

  C: TColor32;
  i: Integer;

  function Tess(aValue: Int64): Int64; inline;
  begin
    // 2nd power = squared, 3rd power = cube, so 4th power = tesserected?
    Result := aValue * aValue * aValue * aValue;
  end;

begin
  // It should be noted that this algorithm is only there so that there's
  // something there for importing TBitmap32's. Where possible, a dedicated
  // palettization tool should be used.

  // First, look for an exact match. If found, there's our result.
  for i := 0 to 127 do
    if fPalette[i] = aColor then
    begin
      Result := i;
      Exit;
    end;

  // If no luck, figure out how close each color is. We have a special case
  // here: Unless none is available, an input color with an alpha of 0 will
  // only be matched to a palette color that also has an alpha of 0.
  HasNullAlpha := false;
  MinDiff := 0;

  for i := 0 to 127 do
  begin
    C := fPalette[i];

    DiffTable[i] := (
                     Tess(RedComponent(C) - RedComponent(aColor)) +
                     Tess(GreenComponent(C) - GreenComponent(aColor)) +
                     Tess(BlueComponent(C) - BlueComponent(aColor))
                    ) * 3;
    AlphaDiffTable[i] := Tess(AlphaComponent(C) - AlphaComponent(aColor));

    HasNullAlpha := HasNullAlpha or (AlphaComponent(C) = 0);
    MinDiff := Max(MinDiff, DiffTable[i] + AlphaDiffTable[i] + 1);
  end;

  if AlphaComponent(aColor) <> 0 then HasNullAlpha := false;

  Result := 0;

  for i := 0 to 127 do
  begin
    if HasNullAlpha and ((fPalette[i] and $FF000000) <> 0) then Continue;

    if DiffTable[i] + AlphaDiffTable[i] < MinDiff then
    begin
      MinDiff := DiffTable[i] + AlphaDiffTable[i];
      Result := i;
    end;
  end;
end;

procedure TPalImage.SetWidth(aValue: Integer);
begin
  SetSize(aValue, Height);
end;

procedure TPalImage.SetHeight(aValue: Integer);
begin
  SetSize(Width, aValue);
end;

function TPalImage.GetWidth: Integer;
begin
  Result := fPixelData.Width;
end;

function TPalImage.GetHeight: Integer;
begin
  Result := fPixelData.Height;
end;

procedure TPalImage.Changed;
begin
  if Assigned(fOnChange) then fOnChange();
end;

procedure TPalImage.LoadFromFile(aFile: String);
var
  F: TFileStream;
begin
  F := TFileStream.Create(aFile, fmOpenRead);
  try
    LoadFromStream(F);
  finally
    F.Free;
  end;
end;

procedure TPalImage.SaveToFile(aFile: String);
var
  F: TFileStream;
begin
  F := TFileStream.Create(aFile, fmCreate);
  try
    SaveToStream(F);
  finally
    F.Free;
  end;
end;

procedure TPalImage.LoadFromStream(aStream: TStream);
var
  W, H: Word;
begin
  W := 0;
  H := 0;
  LoadPaletteFromStream(aStream, false);
  aStream.Read(W, 2);
  aStream.Read(H, 2);
  fPixelData.SetSize(W, H);
  aStream.Read(fPixelData.Bits^, W * H);
end;

procedure TPalImage.SaveToStream(aStream: TStream);
var
  W, H: Word;
begin
  W := Width;
  H := Height;
  SavePaletteToStream(aStream, false);
  aStream.Write(W, 2);
  aStream.Write(H, 2);
  aStream.Write(fPixelData.Bits^, W * H);
end;

procedure TPalImage.SetSize(aWidth, aHeight: Integer; aAnchor: TPreserveAnchor;
  aSpaceColor: Byte);
var
  OldData: TByteMap;
  X, Y: Integer;
  oX, oY: Integer;
begin
  if (aWidth = Width) and (aHeight = Height) then Exit;

  OldData := fPixelData;
  fPixelData := TByteMap.Create;
  try
    fPixelData.SetSize(aWidth, aHeight);
    fPixelData.Clear(aSpaceColor);

    if aAnchor = paClear then Exit;

    case aAnchor of
      paTopLeft, paLeft, paBottomLeft: oX := 0;
      paTop, paCenter, paBottom: oX := (fPixelData.Width - OldData.Width) div 2;
      paTopRight, paRight, paBottomRight: oX := fPixelData.Width - OldData.Width;
    end;

    case aAnchor of
      paTopLeft, paTop, paTopRight: oY := 0;
      paLeft, paCenter, paRight: oY := (fPixelData.Height - OldData.Height) div 2;
      paBottomLeft, paBottom, paBottomRight: oY := fPixelData.Height - OldData.Height;
    end;

    for Y := 0 to OldData.Height-1 do
    begin
      if (Y + oY < 0) or (Y + oY >= fPixelData.Height) then Continue;

      for X := 0 to OldData.Width-1 do
      begin
        if (X + oX < 0) or (X + oX >= fPixelData.Width) then Continue;

        fPixelData[X + oX, Y + oY] := OldData[X, Y];
      end;
    end;
  finally
    OldData.Free;
  end;

  Changed;
end;

procedure TPalImage.Clear(aIndex: Byte);
begin
  fPixelData.Clear(aIndex);
  Changed;
end;

procedure TPalImage.ClearColor(aColor: TColor32);
begin
  Clear(GetEquivalentPaletteIndex(aColor));
end;

procedure TPalImage.LoadPaletteFromFile(aFile: String; aTextBased: Boolean);
var
  F: TFileStream;
begin
  F := TFileStream.Create(aFile, fmOpenRead);
  try
    LoadPaletteFromStream(F, aTextBased);
  finally
    F.Free;
  end;
end;

procedure TPalImage.SavePaletteToFile(aFile: String; aTextBased: Boolean);
var
  F: TFileStream;
begin
  F := TFileStream.Create(aFile, fmCreate);
  try
    SavePaletteToStream(F, aTextBased);
  finally
    F.Free;
  end;
end;

procedure TPalImage.LoadPaletteFromStream(aStream: TStream; aTextBased: Boolean);
var
  SL: TStringList;
  i: Integer;
begin
  FillChar(fPalette, 128 * 4, 0);

  if aTextBased then
  begin
    SL := TStringList.Create;

    try
      SL.LoadFromStream(aStream);

      i := 0;
      while (SL.Count > 0) and (i < 128) do
      begin
        if LeftStr(SL[0], 1) <> ';' then
        begin
          fPalette[i] := StrToInt('x' + SL[0]);
          Inc(i);
        end;

        SL.Delete(0);
      end;
    finally
      SL.Free;
    end;
  end else
    aStream.Read(fPalette[0], 128 * 4);

  Changed;
end;

procedure TPalImage.SavePaletteToStream(aStream: TStream; aTextBased: Boolean);
var
  SL: TStringList;
  i: Integer;
begin
  if aTextBased then
  begin
    SL := TStringList.Create;
    try
      for i := 0 to 127 do
        SL.Add(IntToHex(fPalette[i], 8));
      SL.SaveToStream(aStream);
    finally
      SL.Free;
    end;
  end else
    aStream.Write(fPalette[0], 128 * 4);
end;

procedure TPalImage.LoadPaletteFromImage(aSource: TPalImage);
begin
  fPalette := aSource.fPalette;
  Changed;
end;

procedure TPalImage.SetPaletteToDefault;
const
  PALETTE_PATTERN = 'FFF|QQQ|FFH|FFN|FHF|FHH|FHN|FNF|FNH|FNN|HFF|HFH|HFN|HHF|HNF|NFF|' +
                    'NFH|NFN|NHF|NNF|FFQ|FQF|FQQ|FQN|FNQ|QFF|QFQ|QFN|QQF|QNF|NFQ|NQF';
  FULL_VALUES: array[0..3] of Integer = (240, 192, 144, 96);
  QUARTER_VALUES: array[0..3] of Integer = (160, 128, 96, 64);
  HALF_VALUES: array[0..3] of Integer = (80, 64, 48, 32);
var
  SL: TStringList;
  i, v, n: Integer;
  ThisPattern: String;

  NewPalette: array[0..127] of TColor32;

  function GetValue(aLetter: Char; aVariant: Integer): Integer;
  begin
    case aLetter of
      'F': Result := FULL_VALUES[aVariant mod 4];
      'Q': Result := QUARTER_VALUES[aVariant mod 4];
      'H': Result := HALF_VALUES[aVariant mod 4];
      'N': Result := 0;
      else raise Exception.Create('Invalid input in palette pattern.');
    end;
  end;

  procedure CopySorted;
  var
    SortIndex: array[0..31] of Integer;
    H, S, V: Single;
    Done: array[0..31] of Boolean;
    NewOrder: array[0..31] of Integer;
    LowestValue, HighestValue: Integer;
    BestIndex: Integer;
    i, n: Integer;
  begin
    for i := 0 to 31 do
      Done[i] := false;

    HighestValue := 0;

    for i := 0 to 31 do
      if i >= 2 then
      begin
        RGBToHSV(NewPalette[i * 4], H, S, V);
        SortIndex[i] := (Trunc(H * 255) shl 16) +
                        (Trunc((1 - S) * 255) shl 0) +
                        (Trunc((1 - V) * 255) shl 8);
        if SortIndex[i] > HighestValue then HighestValue := SortIndex[i];
      end else
        SortIndex[i] := -2;

    n := 0;
    while n < 32 do
    begin
      if SortIndex[n] = -2 then
      begin
        NewOrder[n] := n;
        Done[n] := true;
      end else begin
        LowestValue := HighestValue + 1;
        NewOrder[n] := -1;

        for i := 2 to 31 do
          if (not Done[i]) and (SortIndex[i] < LowestValue) then
          begin
            LowestValue := SortIndex[i];
            NewOrder[n] := i;
          end;

        Done[NewOrder[n]] := true;
      end;
      Inc(n);
    end;

    fPalette[0] := NewPalette[0];

    for i := 1 to 7 do
    begin
      HighestValue := -1;
      BestIndex := -1;
      for n := 1 to 7 do
        if ((NewPalette[n] and $FF) > HighestValue) and
           ((NewPalette[n] and $FF000000) > 0) then
        begin
          BestIndex := n;
          fPalette[i] := NewPalette[n];
          HighestValue := NewPalette[n] and $FF;
        end;
      NewPalette[BestIndex] := $00000000;
    end;


    for i := 2 to 31 do
      for n := 0 to 3 do
        if i < 17 then
          fPalette[(i - 1) * 8 + n] := NewPalette[NewOrder[i] * 4 + n]
        else
          fPalette[(i - 1) * 8 + n - 116] := NewPalette[NewOrder[i] * 4 + n];
  end;

begin
  SL := TStringList.Create;
  try
    SL.Delimiter := '|';
    SL.StrictDelimiter := true;
    SL.DelimitedText := PALETTE_PATTERN;

    for i := 0 to 127 do
      NewPalette[i] := $FF000000;

    v := 0;
    n := 0;
    for i := 0 to 127 do
    begin
      ThisPattern := SL[n];

      NewPalette[i] := (GetValue(ThisPattern[1], v) shl 16) or
                       (GetValue(ThisPattern[2], v) shl 8) or
                       (GetValue(ThisPattern[3], v)) or
                       $FF000000;

      Inc(v);
      if v = 4 then
      begin
        v := 0;
        Inc(n);
      end;
    end;

    // Special cases
    NewPalette[3] := NewPalette[5];
    NewPalette[4] := NewPalette[6];
    NewPalette[5] := NewPalette[7];
    NewPalette[6] := $FF000000;

    for i := 7 downto 1 do
      NewPalette[i] := NewPalette[i - 1];

    NewPalette[0] := $00000000;
  finally
    SL.Free;
  end;

  CopySorted;

  Changed;
end;

procedure TPalImage.WriteBitmap32(Dst: TBitmap32);
begin
  Dst.SetSize(Width, Height);
  WriteBitmap32(Dst, Point(0, 0));
end;

procedure TPalImage.WriteBitmap32(Dst: TBitmap32; aOrigin: TPoint);
var
  x, y: Integer;
begin
  for y := 0 to Height-1 do
    for x := 0 to Width-1 do
      Dst.PixelS[x + aOrigin.X, y + aOrigin.Y] := fPalette[fPixelData[x, y]];
end;

procedure TPalImage.ReadBitmap32(Src: TBitmap32);
begin
  ReadBitmap32(Src, Src.BoundsRect);
end;

procedure TPalImage.ReadBitmap32(Src: TBitmap32; aRegion: TRect);
var
  x, y: Integer;
begin
  fPixelData.SetSize(aRegion.Width, aRegion.Height);
  for y := 0 to aRegion.Height-1 do
    for x := 0 to aRegion.Width-1 do
      fPixelData[x, y] := GetEquivalentPaletteIndex(Src.PixelS[x + aRegion.Left, y + aRegion.Top]);
  Changed;
end;

end.

