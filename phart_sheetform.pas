unit phart_sheetform;

// Copyright (c) 2020 Namida Verasche
// Phart source code is made available under the MIT Licence
// See readme.txt for more information

{$mode objfpc}{$H+}

interface

uses
  phart_sheetsizeform, phart_sheethelp, palimage, Math, GR32_Resamplers, GR32,
  GR32_Png, FGL, Classes, SysUtils, Forms, Controls, Graphics, Dialogs,
  StdCtrls, ExtCtrls, GR32_Image, GR32_Layers;

type

  TImage32List = specialize TFPGObjectList<TImage32>;

  { TFSheetManager }

  TFSheetManager = class(TForm)
    imgFromEdit: TImage32;
    imgHelp: TImage32;
    imgNew: TImage32;
    imgOpen: TImage32;
    imgResize: TImage32;
    imgSave: TImage32;
    imgSelf: TImage32;
    imgSheet: TImage32;
    imgSample: TImage32;
    imgToEdit: TImage32;
    imgToSample: TImage32;
    odOpenImage: TOpenDialog;
    pnButtons: TPanel;
    sbScrollImage: TScrollBar;
    sdSaveImage: TSaveDialog;

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);

    procedure imgSampleMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);

    procedure imgSheetMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);

    procedure sbScrollImageScroll(Sender: TObject; ScrollCode: TScrollCode;
      var ScrollPos: Integer);

    // Buttons
    procedure imgButtonMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
    procedure imgButtonMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
    procedure imgButtonMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer; Layer: TCustomLayer);
  private
    fActiveButton: TImage32; 
    fButtonList: TImage32List;

    fSampleGrid: array[0..2, 0..2] of Integer;

    fFilename: String;
    fSaved: Boolean;

    fSampleFreeVertSpace: Integer;
    fSheetImage: TPalImage;

    fPieceWidth: Integer;
    fPieceHeight: Integer;

    fSelectedPiece: Integer;

    function ConfirmSaved: Boolean;

    procedure PrepareToolButtons;

    procedure DrawSheetImage;
    procedure SetSheetScaling;
    procedure DrawSampleImage;
    procedure SetSampleScaling;

    procedure New;
    procedure Open;
    procedure Save(SaveAs: Boolean);

    procedure ToEdit;
    procedure FromEdit;
    procedure ToSample;

    procedure ResizeSheet;
    procedure SetSelfPiece;

    function GetSelectedPieceRect: TRect;
    function GetPieceRect(aIndex: Integer): TRect;
  public
    procedure SetPaletteFrom(aSource: TPalImage);
  end;

var
  FSheetManager: TFSheetManager;

implementation

uses
  phart_main;

type
  TBitmap32List = specialize TFPGObjectList<TBitmap32>;

const
  BTN_TAG_NEW = 0;
  BTN_TAG_OPEN = 1;
  BTN_TAG_SAVE = 2;

  BTN_TAG_TO_EDIT = 3;
  BTN_TAG_FROM_EDIT = 4;
  BTN_TAG_TO_SAMPLE = 5;

  BTN_TAG_RESIZE = 6;

  BTN_TAG_SELF = 7;

  BTN_TAG_HELP = 8;

{$R *.lfm}

{ TFSheetManager }

procedure TFSheetManager.FormShow(Sender: TObject);
begin
  Left := Min(Screen.Width - Width, FPhart.Left + FPhart.Width - Width + 64);
  Top := FPhart.Top;
  Width := Constraints.MinWidth;
  Height := Constraints.MinHeight;
end;

procedure TFSheetManager.imgSampleMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
var
  DstPt: TPoint;
begin
  if (fPieceWidth = 0) or (fPieceHeight = 0) then Exit;

  DstPt := imgSample.ControlToBitmap(Point(X, Y));
  DstPt.X := DstPt.X div fPieceWidth;
  DstPt.Y := DstPt.Y div fPieceHeight;

  if (DstPt.X < 0) or (DstPt.X > 2) or (DstPt.Y < 0) or (DstPt.Y > 2) then Exit;

  if Button = mbRight then
    fSampleGrid[DstPt.X, DstPt.Y] := -2
  else
    fSampleGrid[DstPt.X, DstPt.Y] := fSelectedPiece;

  DrawSampleImage;
end;

procedure TFSheetManager.imgSheetMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
var
  TouchPoint: TPoint;
begin
  if (fPieceWidth = 0) or (fPieceHeight = 0) then Exit;

  TouchPoint := imgSheet.ControlToBitmap(Point(X, Y));
  fSelectedPiece := (TouchPoint.X div fPieceWidth) +
                    ((TouchPoint.Y div fPieceHeight) * (fSheetImage.Width div fPieceWidth));
  imgSelf.OffsetVert := 0;

  DrawSheetImage;
  DrawSampleImage;
end;

procedure TFSheetManager.DrawSheetImage;
var
  SelRect: TRect;
begin
  imgSheet.Bitmap.BeginUpdate;
  try
    fSheetImage.WriteBitmap32(imgSheet.Bitmap);

    if fSelectedPiece >= 0 then
    begin
      SelRect := GetSelectedPieceRect;

      imgSheet.Bitmap.FrameRectS(SelRect, $FFFF00FF);

      SelRect.Left := SelRect.Left + 1;
      SelRect.Top := SelRect.Top + 1;
      SelRect.Right := SelRect.Right - 1;
      SelRect.Bottom := SelRect.Bottom - 1;

      imgSheet.Bitmap.FrameRectS(SelRect, $FFCC00CC);
    end;
  finally
    imgSheet.Bitmap.EndUpdate;
    imgSheet.Invalidate;
  end;
end;

procedure TFSheetManager.SetSheetScaling;
begin
  if fSheetImage.Width > 0 then
  begin
    if (imgSheet.Width) >= fSheetImage.Width then
      imgSheet.Scale := (imgSheet.Width) div fSheetImage.Width
    else
      imgSheet.Scale := 1 / ((fSheetImage.Width div imgSheet.Width) + 1);

    if imgSheet.Scale < 1 then
      TLinearResampler.Create(imgSheet.Bitmap)
    else
      TNearestResampler.Create(imgSheet.Bitmap);

    sbScrollImage.Enabled := imgSheet.Bitmap.Height * imgSheet.Scale > imgSheet.Height;
    sbScrollImage.Min := 0;
    sbScrollImage.Max := Max(0, Trunc(imgSheet.Bitmap.Height * imgSheet.Scale - imgSheet.Height));

    sbScrollImage.SmallChange := Max(1, sbScrollImage.Max div sbScrollImage.Height * 4);
    sbScrollImage.LargeChange := sbScrollImage.SmallChange * 4;
    sbScrollImage.PageSize := sbScrollImage.LargeChange;

    sbScrollImage.Max := sbScrollImage.Max + sbScrollImage.LargeChange;
  end else begin
    sbScrollImage.Enabled := false;
    sbScrollImage.Min := 0;
    sbScrollImage.Max := 0;
  end;
end;

procedure TFSheetManager.DrawSampleImage;
var
  BMP: TBitmap32;
  x, y: Integer;
  SrcRect: TRect;
begin
  imgSample.BeginUpdate;
  BMP := TBitmap32.Create;
  try
    imgSample.Bitmap.SetSize(fPieceWidth * 3, fPieceHeight * 3);
    imgSample.Bitmap.Clear(0);

    fSheetImage.WriteBitmap32(BMP);

    for y := 0 to 2 do
      for x := 0 to 2 do
      begin
        if fSampleGrid[x, y] = -2 then
          Continue
        else if (fSampleGrid[x, y] = -1) and (fSelectedPiece = -1) then
        begin
          imgSample.Bitmap.FillRect(x * fPieceWidth, y * fPieceHeight, (x+1) * fPieceWidth, (y+1) * fPieceHeight, $FFFF00FF);
          Continue;
        end else if fSampleGrid[x, y] = -1 then
          SrcRect := GetSelectedPieceRect
        else
          SrcRect := GetPieceRect(fSampleGrid[x, y]);

        BMP.DrawTo(imgSample.Bitmap, x * fPieceWidth, y * fPieceHeight, SrcRect);
      end;

    SetSampleScaling;
  finally
    BMP.Free;
    imgSample.EndUpdate;
    imgSample.Invalidate;
  end;
end;

procedure TFSheetManager.SetSampleScaling;
var
  MaxScale: Single;
begin
  if (imgSample.Bitmap.Width = 0) or (imgSample.Bitmap.Height = 0) then Exit;

  MaxScale := Min(imgSample.Width / imgSample.Bitmap.Width, imgSample.Height / imgSample.Bitmap.Height);

  if MaxScale < 1 then
  begin
    if imgSample.Scale >= 1 then
      TLinearResampler.Create(imgSample.Bitmap);
    imgSample.Scale := 1 / (Trunc(1 / MaxScale) + 1);
  end else begin
    if imgSample.Scale < 1 then
      TNearestResampler.Create(imgSample.Bitmap);
    imgSample.Scale := Trunc(MaxScale);
  end;
end;

procedure TFSheetManager.New;
var
  x, y: Integer;
begin
  if FSheetSize.ShowNew = mrOk then
  begin
    if not ConfirmSaved then Exit;

    fSheetImage.SetSize(FSheetSize.PiecesHorz * FSheetSize.PieceWidth,
                        FSheetSize.PiecesVert * FSheetSize.PieceHeight);

    fPieceWidth := FSheetSize.PieceWidth;
    fPieceHeight := FSheetSize.PieceHeight;

    DrawSheetImage;
    SetSheetScaling;

    for y := 0 to 2 do
      for x := 0 to 2 do
        fSampleGrid[x, y] := -2;

    DrawSampleImage;

    fFilename := '';
    fSaved := true;
  end;
end;

procedure TFSheetManager.Open;
var
  BMP: TBitmap32;
  TempImg: TPalImage;
  F: TFileStream;
  FHorz, FVert: Integer;
begin
  if odOpenImage.Execute then
  begin
    TempImg := TPalImage.Create;
    try
      case odOpenImage.FilterIndex of
        1: begin       
             TempImg.LoadPaletteFromImage(fSheetImage);

             BMP := TBitmap32.Create;
             try
               LoadBitmap32FromPNG(BMP, odOpenImage.Filename);
               TempImg.ReadBitmap32(BMP);
             finally
               BMP.Free;
             end;
           end;
        2: begin         
             TempImg.LoadPaletteFromImage(fSheetImage);

             BMP := TBitmap32.Create;
             try
               BMP.LoadFromFile(odOpenImage.Filename);
               TempImg.ReadBitmap32(BMP);
             finally
               BMP.Free;
             end;

             TempImg.LoadPaletteFromImage(fSheetImage);
           end;
        3: TempImg.LoadFromFile(odOpenImage.Filename);
        4: begin
             F := TFileStream.Create(odOpenImage.Filename, fmOpenRead);
             try
               TempImg.LoadFromStream(F);
               F.Read(FHorz, 4);
               F.Read(FVert, 4);
             finally
               F.Free;
             end;
           end;
      end;

      if odOpenImage.FilterIndex <> 4 then
      begin
        if FSheetSize.ShowOpen(TempImg.Width, TempImg.Height) = mrCancel then
          Exit;

        if not ConfirmSaved then Exit;

        fSheetImage.Assign(TempImg);

        fPieceWidth := Min(FSheetSize.PieceWidth, fSheetImage.Width div FSheetSize.PiecesHorz);
        fPieceHeight := Min(FSheetSize.PieceHeight, fSheetImage.Height div FSheetSize.PiecesVert);
      end else begin
        if not ConfirmSaved then Exit;

        fSheetImage.Assign(TempImg);
        fPieceWidth := fSheetImage.Width div FHorz;
        fPieceHeight := fSheetImage.Height div FVert;
      end;      

      fSheetImage.SetSize(FSheetSize.PiecesHorz * fPieceWidth,
                          FSheetSize.PiecesVert * fPieceHeight,
                          paTopLeft);

      DrawSheetImage;
      DrawSampleImage;
      SetSheetScaling;

      fFilename := odOpenImage.Filename;
      fSaved := true;
    finally
      TempImg.Free;
    end;
  end;
end;

procedure TFSheetManager.Save(SaveAs: Boolean);
var
  BMP: TBitmap32;
  Ext: String;
  F: TFileStream;
  Temp: Integer;
begin
  if (fFilename = '') or SaveAs then
  begin
    if not sdSaveImage.Execute then Exit;
    fFilename := sdSaveImage.Filename;
  end;

  Ext := Uppercase(ExtractFileExt(fFilename));

  if (Ext <> '.BMP') and (Ext <> '.PNG') and (Ext <> '.PHT') then
  begin
    F := TFileStream.Create(fFilename, fmCreate);
    try
      fSheetImage.SaveToStream(F);
      Temp := fSheetImage.Width div fPieceWidth;
      F.Write(Temp, 4);
      Temp := fSheetImage.Height div fPieceHeight;
      F.Write(Temp, 4);
    finally
      F.Free;
    end;
  end else if (Ext = '.PHT') then
    fSheetImage.SaveToFile(fFilename)
  else begin
    BMP := TBitmap32.Create;
    fSheetImage.WriteBitmap32(BMP);
    if Ext <> '.BMP' then
      SaveBitmap32ToPNG(BMP, fFilename)
    else
      BMP.SaveToFile(fFilename);
  end;

  fSaved := true;
end;

procedure TFSheetManager.ToEdit;
var
  TempImage: TPalImage;
  BMP: TBitmap32;
  MS: TMemoryStream;
begin
  TempImage := TPalImage.Create;
  BMP := TBitmap32.Create;
  MS := TMemoryStream.Create;
  try
    fSheetImage.WriteBitmap32(BMP);
    TempImage.LoadPaletteFromImage(fSheetImage);
    TempImage.ReadBitmap32(BMP, GetSelectedPieceRect);
    TempImage.SaveToStream(MS);

    MS.Position := 0;
    FPhart.LoadDirect(MS);
  finally
    TempImage.Free;
    BMP.Free;
    MS.Free;
  end;
end;

procedure TFSheetManager.FromEdit;
var
  TempImage: TPalImage;
  BMP: TBitmap32;
  MS: TMemoryStream;
begin
  TempImage := TPalImage.Create;
  BMP := TBitmap32.Create;
  MS := TMemoryStream.Create;
  try
    FPhart.SaveDirect(MS);

    MS.Position := 0;
    TempImage.LoadFromStream(MS);

    fSheetImage.LoadPaletteFromImage(TempImage);

    fSheetImage.WriteBitmap32(BMP);
    TempImage.WriteBitmap32(BMP, GetSelectedPieceRect.TopLeft);

    fSheetImage.ReadBitmap32(BMP);

    DrawSheetImage;
    DrawSampleImage;
    SetSheetScaling;
    fSaved := false;
  finally
    TempImage.Free;
    BMP.Free;
    MS.Free;
  end;
end;

procedure TFSheetManager.ToSample;
var
  i: Integer;
  ClonePositions: Integer;
begin
  DrawSampleImage; // just in case

  ClonePositions := 0;
  for i := 0 to 8 do
    if fSampleGrid[i mod 3, i div 3] = -1 then
      ClonePositions := ClonePositions or (1 shl i);

  FPhart.LoadSampleDirect(imgSample.Bitmap, ClonePositions);
end;

procedure TFSheetManager.ResizeSheet;
var
  x, y: Integer;
  OldHorzCount, WriteHorzCount: Integer;
  BMP: TBitmap32;

  BMPList: TBitmap32List;
  NewBMP: TBitmap32;

  SrcRect: TRect;
begin
  if FSheetSize.ShowResize(fSheetImage.Width div fPieceWidth,
                           fSheetImage.Height div fPieceHeight,
                           fPieceWidth, fPieceHeight) = mrOK then
  begin
    BMP := TBitmap32.Create;
    BMPList := TBitmap32List.Create;
    try
      fSheetImage.WriteBitmap32(BMP);

      for y := 0 to (fSheetImage.Height div fPieceHeight) - 1 do
        for x := 0 to (fSheetImage.Width div fPieceWidth) - 1 do
        begin
          NewBMP := TBitmap32.Create;
          BMPList.Add(NewBMP);

          NewBMP.SetSize(fPieceWidth, fPieceHeight);
          BMP.DrawTo(NewBMP, 0, 0, Rect(x * fPieceWidth, y * fPieceHeight, (x+1) * fPieceWidth, (y+1) * fPieceHeight));
        end;

      SrcRect := Rect(0, 0, Min(fPieceWidth, FSheetSize.PieceWidth), Min(fPieceHeight, FSheetSize.PieceHeight));

      OldHorzCount := fSheetImage.Width div fPieceWidth;
      fPieceWidth := FSheetSize.PieceWidth;
      fPieceHeight := FSheetSize.PieceHeight;

      BMP.SetSize(FSheetSize.PiecesHorz * fPieceWidth, FSheetSize.PiecesVert * fPieceHeight);
      BMP.Clear($00000000);

      WriteHorzCount := OldHorzCount;

      for y := 0 to (BMP.Height div fPieceHeight) - 1 do
      begin
        for x := 0 to (BMP.Width div fPieceWidth) - 1 do
        begin
          if (WriteHorzCount = 0) and not FSheetSize.PreserveIndexes then Break;
          if BMPList.Count = 0 then Break;
          BMPList[0].DrawTo(BMP, x * fPieceWidth, y * fPieceHeight, SrcRect);
          BMPList.Delete(0);
          Dec(WriteHorzCount);
        end;

        if not FSheetSize.PreserveIndexes then
          while (WriteHorzCount > 0) and (BMPList.Count > 0) do
          begin
            Dec(WriteHorzCount);
            BMPList.Delete(0);
          end;

        WriteHorzCount := OldHorzCount;
      end;

      fSheetImage.ReadBitmap32(BMP);
    finally
      BMP.Free;
      BMPList.Free;
    end;

    fSaved := false;
                  
    DrawSheetImage;
    DrawSampleImage;
    SetSheetScaling;
  end;
end;

procedure TFSheetManager.SetSelfPiece;
begin
  fSelectedPiece := -1;
  imgSelf.OffsetVert := -32 * Screen.PixelsPerInch / 96;
  DrawSheetImage;
  DrawSampleImage;
end;

function TFSheetManager.GetSelectedPieceRect: TRect;
begin
  Result := GetPieceRect(fSelectedPiece);
end;

function TFSheetManager.GetPieceRect(aIndex: Integer): TRect;
var
  RowPieceCount: Integer;
begin
  RowPieceCount := fSheetImage.Width div fPieceWidth;
  Result := Rect(aIndex mod RowPieceCount * fPieceWidth, aIndex div RowPieceCount * fPieceHeight,
                      0, 0);
  Result.Right := Result.Left + fPieceWidth;
  Result.Bottom := Result.Top + fPieceHeight;
end;

procedure TFSheetManager.SetPaletteFrom(aSource: TPalImage);
begin
  fSheetImage.LoadPaletteFromImage(aSource);
end;

procedure TFSheetManager.FormCreate(Sender: TObject);
var
  x, y: Integer;
begin
  Constraints.MinWidth := Width;
  Constraints.MinHeight := Height;
  fSampleFreeVertSpace := ClientHeight - imgSample.Height;

  fSheetImage := TPalImage.Create;

  fButtonList := TImage32List.Create(false);
  PrepareToolButtons;

  fSaved := true;

  for y := 0 to 2 do
    for x := 0 to 2 do
      fSampleGrid[x, y] := -2;

  FormResize(self);
end;

procedure TFSheetManager.FormDestroy(Sender: TObject);
begin
  fSheetImage.Free;
  fButtonList.Free;
end;

procedure TFSheetManager.FormResize(Sender: TObject);
var
  AvailableWidth: Integer;
  DPIFactor: Single;
begin
  DPIFactor := Screen.PixelsPerInch / 96;

  imgSheet.Height := ClientHeight - Trunc(16 * DPIFactor);
  AvailableWidth := ClientWidth - sbScrollImage.Width - Trunc(32 * DPIFactor);
  imgSheet.Width := AvailableWidth * 8 div 11;
  imgSample.Width := Min(ClientWidth - Trunc(32 * DPIFactor) - sbScrollImage.Width - imgSheet.Width, ClientHeight - fSampleFreeVertSpace);
  imgSample.Height := imgSample.Width;
  imgSample.Left := (((ClientWidth - imgSheet.Width - Trunc(32 * DPIFactor)) - imgSample.Width) div 2) + imgSheet.Left + imgSheet.Width + Trunc(16 * DPIFactor);
  sbScrollImage.Left := imgSheet.Left + imgSheet.Width;

  pnButtons.Left := imgSample.Left + ((imgSample.Width - pnButtons.Width) div 2);
  pnButtons.Top := imgSample.Top + imgSample.Height + Trunc(8 * DPIFactor);

  SetSheetScaling;
  SetSampleScaling;
end;

procedure TFSheetManager.imgButtonMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
var
  Btn: TImage32 absolute Sender;
begin
  Btn.OffsetVert := -32 * Screen.PixelsPerInch / 96;
  fActiveButton := Btn;
end;

procedure TFSheetManager.imgButtonMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
var
  Btn: TImage32 absolute Sender;
begin
  Btn.OffsetVert := 0;
  if fActiveButton <> Btn then Exit;

  fActiveButton := nil;

  case Btn.Tag of
    BTN_TAG_NEW: New;
    BTN_TAG_OPEN: Open;
    BTN_TAG_SAVE: Save(Button = mbRight);

    BTN_TAG_TO_EDIT: ToEdit;
    BTN_TAG_FROM_EDIT: FromEdit;
    BTN_TAG_TO_SAMPLE: ToSample;

    BTN_TAG_RESIZE: ResizeSheet;

    BTN_TAG_SELF: SetSelfPiece;

    BTN_TAG_HELP: FSheetManagerHelp.ShowModal;
  end;
end;

procedure TFSheetManager.imgButtonMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer; Layer: TCustomLayer);
var
  Btn: TImage32 absolute Sender;
begin
  if fActiveButton <> Btn then Exit;
  if not PtInRect(Rect(0, 0, 32, 32), Point(X, Y)) then
  begin
    Btn.OffsetVert := 0;
    fActiveButton := nil;
  end;
end;

procedure TFSheetManager.sbScrollImageScroll(Sender: TObject;
  ScrollCode: TScrollCode; var ScrollPos: Integer);
begin 
  imgSheet.OffsetVert := -sbScrollImage.Position;
end;

function TFSheetManager.ConfirmSaved: Boolean;
begin
  if fSaved then
    Result := true
  else begin
    case MessageDlg('Save?', 'Do you want to save this sheet?', mtCustom, [mbYes, mbNo, mbCancel], 0) of
      mrYes: begin Save(false); Result := fSaved; end;
      mrNo: Result := true;
      else Result := false;
    end;
  end;
end;

procedure TFSheetManager.PrepareToolButtons;
var
  SrcBitmap: TBitmap32;
  R: TResourceStream;

  procedure SetupButton(aTarget: TImage32; aImageIndex: Integer; aTag: Integer);
  var
    BMP: TBitmap32;
    SrcRect: TRect;
  begin
    BMP := aTarget.Bitmap;
    BMP.SetSize(32, 64);

    SrcBitmap.DrawTo(BMP, 0, 0, Rect(0, 0, 32, 32));
    SrcBitmap.DrawTo(BMP, 0, 32, Rect(32, 0, 64, 32));

    SrcRect := Rect((aImageIndex mod 10) * 32, (aImageIndex div 10) * 32, 0, 0);
    SrcRect.Right := SrcRect.Left + 32;
    SrcRect.Bottom := SrcRect.Top + 32;

    SrcBitmap.DrawTo(BMP, 0, 0, SrcRect);
    SrcBitmap.DrawTo(BMP, 0, 32, SrcRect);

    aTarget.Tag := aTag;
    aTarget.Scale := Screen.PixelsPerInch / 96;
    if Screen.PixelsPerInch mod 96 <> 0 then
      TLinearResampler.Create(BMP);

    fButtonList.Add(aTarget);
  end;
begin
  SrcBitmap := TBitmap32.Create;
  try
    R := TResourceStream.Create(HInstance, 'UI', 'BUTTONS');
    try
      LoadBitmap32FromPng(SrcBitmap, R);
    finally
      R.Free;
    end;

    SrcBitmap.DrawMode := dmBlend;

    SetupButton(imgNew, 2, BTN_TAG_NEW);
    SetupButton(imgOpen, 3, BTN_TAG_OPEN);
    SetupButton(imgSave, 4, BTN_TAG_SAVE);

    SetupButton(imgToEdit, 26, BTN_TAG_TO_EDIT);
    SetupButton(imgFromEdit, 27, BTN_TAG_FROM_EDIT);
    SetupButton(imgToSample, 28, BTN_TAG_TO_SAMPLE);

    SetupButton(imgResize, 13, BTN_TAG_RESIZE);

    SetupButton(imgSelf, 29, BTN_TAG_SELF);

    SetupButton(imgHelp, 22, BTN_TAG_HELP);
  finally
    SrcBitmap.Free;
  end;
end;

end.

