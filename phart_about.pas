unit phart_about;

// Copyright (c) 2020 Namida Verasche
// Phart source code is made available under the MIT Licence
// See readme.txt for more information

{$mode objfpc}{$H+}

interface

uses
  GR32, GR32_Png, GR32_Resamplers,
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, GR32_Image;

type

  { TFAboutPhart }

  TFAboutPhart = class(TForm)
    btnOK: TButton;
    imgCopy: TImage32;
    imgCut: TImage32;
    imgFill: TImage32;
    imgHelp: TImage32;
    imgLoadSample: TImage32;
    imgSheetManager: TImage32;
    imgNew: TImage32;
    imgOpen: TImage32;
    imgPan: TImage32;
    imgPanSample: TImage32;
    imgPaste: TImage32;
    imgPencil: TImage32;
    imgPicker: TImage32;
    imgQuickPalette: TImage32;
    imgRedo: TImage32;
    imgResize: TImage32;
    imgSave: TImage32;
    imgSelect: TImage32;
    imgUndo: TImage32;
    imgZoom: TImage32;
    imgZoomSample: TImage32;
    lblLicence: TLabel;
    lblNew: TLabel;
    lblPanSample: TLabel;
    lblCenterSample: TLabel;
    lblResize: TLabel;
    lblPencil: TLabel;
    lblFill: TLabel;
    lblPicker: TLabel;
    lblSelect: TLabel;
    lblCut: TLabel;
    lblCopy: TLabel;
    lblPaste: TLabel;
    lblOpen: TLabel;
    lblQuickPalette: TLabel;
    lblHelp: TLabel;
    lblEditPalette: TLabel;
    lblAbout: TLabel;
    lblSave: TLabel;
    lblOpenSample: TLabel;
    lblUndo: TLabel;
    lblRedo: TLabel;
    lblPanImage: TLabel;
    lblSheetManager: TLabel;
    lblZoomImage: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    procedure PrepareToolButtons;
  public
    procedure SetVersion(aVersion: String);
  end;

var
  FAboutPhart: TFAboutPhart;

implementation

{$R *.lfm}

procedure TFAboutPhart.FormCreate(Sender: TObject);
begin
  PrepareToolButtons;
end;

procedure TFAboutPhart.PrepareToolButtons;
var
  SrcBitmap: TBitmap32;
  R: TResourceStream;

  procedure SetupButton(aTarget: TImage32; aImageIndex: Integer);
  var
    BMP: TBitmap32;
    SrcRect: TRect;
  begin
    BMP := aTarget.Bitmap;
    BMP.SetSize(32, 32);

    SrcBitmap.DrawTo(BMP, 0, 0, Rect(0, 0, 32, 32));

    SrcRect := Rect((aImageIndex mod 10) * 32, (aImageIndex div 10) * 32, 0, 0);
    SrcRect.Right := SrcRect.Left + 32;
    SrcRect.Bottom := SrcRect.Top + 32;

    SrcBitmap.DrawTo(BMP, 0, 0, SrcRect);

    if Screen.PixelsPerInch mod 96 <> 0 then
      TLinearResampler.Create(BMP);
  end;
begin
  SrcBitmap := TBitmap32.Create;
  try
    R := TResourceStream.Create(HInstance, 'UI', 'BUTTONS');
    try
      LoadBitmap32FromPng(SrcBitmap, R);
    finally
      R.Free;
    end;

    SrcBitmap.DrawMode := dmBlend;

    SetupButton(imgNew, 2);
    SetupButton(imgOpen, 3);
    SetupButton(imgSave, 4);
    SetupButton(imgLoadSample, 6);

    SetupButton(imgSheetManager, 25);

    SetupButton(imgUndo, 7);
    SetupButton(imgRedo, 8);

    SetupButton(imgPan, 9);
    SetupButton(imgZoom, 10);
    SetupButton(imgPanSample, 11);
    SetupButton(imgZoomSample, 12);

    SetupButton(imgResize, 13);

    SetupButton(imgPencil, 14);
    SetupButton(imgFill, 15);
    SetupButton(imgPicker, 16);

    SetupButton(imgSelect, 17);
    SetupButton(imgCut, 19);
    SetupButton(imgCopy, 20);
    SetupButton(imgPaste, 21);

    SetupButton(imgQuickPalette, 18);

    SetupButton(imgHelp, 22);
  finally
    SrcBitmap.Free;
  end;
end;

procedure TFAboutPhart.SetVersion(aVersion: String);
begin
  lblAbout.Caption := StringReplace(lblAbout.Caption, '~', aVersion, [rfReplaceAll]);
end;

end.

