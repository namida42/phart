unit phart_sheetsizeform;

// Copyright (c) 2020 Namida Verasche
// Phart source code is made available under the MIT Licence
// See readme.txt for more information

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, Spin, StdCtrls,
  ExtCtrls;

type

  { TFSheetSize }

  TFSheetSize = class(TForm)
    btnOK: TButton;
    btnCancel: TButton;
    cbPreserveIndexes: TCheckBox;
    lblPieces: TLabel;
    lblPieceSize: TLabel;
    lbWhyDoesThisNeedAName: TLabel;
    lbWhyDoesThisNeedAName2ElectricBoogaloo: TLabel;
    sePieceHeight: TSpinEdit;
    sePiecesHorz: TSpinEdit;
    sePiecesVert: TSpinEdit;
    sePieceWidth: TSpinEdit;
    procedure sePieceHeightChange(Sender: TObject);
    procedure sePiecesHorzChange(Sender: TObject);
    procedure sePiecesVertChange(Sender: TObject);
    procedure sePieceWidthChange(Sender: TObject);
  private
    fSuspendEvents: Boolean;

    fOpenMode: Boolean;
    fOpenWidth: Integer;
    fOpenHeight: Integer;

    function GetPiecesHorzValue: Integer;
    function GetPiecesVertValue: Integer;
    function GetPieceWidthValue: Integer;
    function GetPieceHeightValue: Integer;
    function GetPreserveIndexes: Boolean;
  public
    function ShowNew: Integer;
    function ShowOpen(aWidth, aHeight: Integer): Integer;
    function ShowResize(aPiecesHorz, aPiecesVert, aPieceWidth, aPieceHeight: Integer): Integer;

    property PiecesHorz: Integer read GetPiecesHorzValue;
    property PiecesVert: Integer read GetPiecesVertValue;
    property PieceWidth: Integer read GetPieceWidthValue;
    property PieceHeight: Integer read GetPieceHeightValue;
    property PreserveIndexes: Boolean read GetPreserveIndexes;
  end;

var
  FSheetSize: TFSheetSize;

implementation

{$R *.lfm}

{ TFSheetSize }

procedure TFSheetSize.sePiecesHorzChange(Sender: TObject);
begin
  if fSuspendEvents or not fOpenMode then Exit;

  fSuspendEvents := true;
  try
    sePieceWidth.Value := fOpenWidth div sePiecesHorz.Value;
  finally
    fSuspendEvents := false;
  end;
end;

procedure TFSheetSize.sePiecesVertChange(Sender: TObject);
begin
  if fSuspendEvents or not fOpenMode then Exit;

  fSuspendEvents := true;
  try
    sePieceHeight.Value := fOpenHeight div sePiecesVert.Value;
  finally
    fSuspendEvents := false;
  end;
end;

procedure TFSheetSize.sePieceWidthChange(Sender: TObject);
begin
  if fSuspendEvents or not fOpenMode then Exit;

  fSuspendEvents := true;
  try
    sePiecesHorz.Value := fOpenWidth div sePieceWidth.Value;
  finally
    fSuspendEvents := false;
  end;
end;       

procedure TFSheetSize.sePieceHeightChange(Sender: TObject);
begin
  if fSuspendEvents or not fOpenMode then Exit;

  fSuspendEvents := true;
  try
    sePiecesVert.Value := fOpenHeight div sePieceHeight.Value;
  finally
    fSuspendEvents := false;
  end;
end;

function TFSheetSize.GetPiecesHorzValue: Integer;
begin        
  Result := sePiecesHorz.Value;
end;

function TFSheetSize.GetPiecesVertValue: Integer;
begin          
  Result := sePiecesVert.Value;
end;

function TFSheetSize.GetPieceWidthValue: Integer;
begin
  Result := sePieceWidth.Value;
end;

function TFSheetSize.GetPieceHeightValue: Integer;
begin
  Result := sePieceHeight.Value;
end;

function TFSheetSize.GetPreserveIndexes: Boolean;
begin
  Result := cbPreserveIndexes.Checked;
end;

function TFSheetSize.ShowNew: Integer;
begin
  fOpenMode := false;
  sePiecesHorz.Value := 16;
  sePiecesVert.Value := 8;
  sePieceWidth.Value := 16;
  sePieceHeight.Value := 16;

  cbPreserveIndexes.Enabled := false;
  cbPreserveIndexes.Visible := false;
  cbPreserveIndexes.Checked := false;

  Result := ShowModal;
end;

function TFSheetSize.ShowOpen(aWidth, aHeight: Integer): Integer;
begin
  fOpenMode := true;
  fOpenWidth := aWidth;
  fOpenHeight := aHeight;
  sePiecesHorz.Value := aWidth div 16;
  sePiecesVert.Value := aHeight div 16;
  sePieceWidth.Value := 16;
  sePieceHeight.Value := 16;

  cbPreserveIndexes.Enabled := false;
  cbPreserveIndexes.Visible := false;
  cbPreserveIndexes.Checked := false;

  Result := ShowModal;
end;

function TFSheetSize.ShowResize(aPiecesHorz, aPiecesVert, aPieceWidth, aPieceHeight: Integer): Integer;
begin
  fOpenMode := false;
  sePiecesHorz.Value := aPiecesHorz;
  sePiecesVert.Value := aPiecesVert;
  sePieceWidth.Value := aPieceWidth;
  sePieceHeight.Value := aPieceHeight;

  cbPreserveIndexes.Enabled := true;
  cbPreserveIndexes.Visible := true;
  cbPreserveIndexes.Checked := true;

  Result := ShowModal;
end;

end.

