unit phart_colorform;

// Copyright (c) 2020 Namida Verasche
// Phart source code is made available under the MIT Licence
// See readme.txt for more information

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, StdCtrls,
  Spin, GR32_Image, GR32, GR32_Blend;

type

  { TFSelectColor }

  TFSelectColor = class(TForm)
    btnCancel: TButton;
    btnOK: TButton;
    imgColor: TImage32;
    lblRed: TLabel;
    lblGreen: TLabel;
    lblBlue: TLabel;
    lblAlpha: TLabel;
    seBlue: TSpinEdit;
    seAlpha: TSpinEdit;
    seRed: TSpinEdit;
    seGreen: TSpinEdit;
    tbBlue: TTrackBar;
    tbAlpha: TTrackBar;
    tbRed: TTrackBar;
    tbGreen: TTrackBar;
    procedure seChannelChange(Sender: TObject);
    procedure tbChannelChange(Sender: TObject);
  private
    fSuspendEvents: Boolean;

    procedure GenerateColorImage;
    procedure SetSelectedColor(aValue: TColor32);
    function GetSelectedColor: TColor32;
  public
    property SelectedColor: TColor32 read GetSelectedColor write SetSelectedColor;
  end;

var
  FSelectColor: TFSelectColor;

implementation

{$R *.lfm}

{ TFSelectColor }

procedure TFSelectColor.seChannelChange(Sender: TObject);
begin
  if fSuspendEvents then Exit;

  fSuspendEvents := true;
  try
    tbAlpha.Position := seAlpha.Value;
    tbRed.Position := seRed.Value;
    tbGreen.Position := seGreen.Value;
    tbBlue.Position := seBlue.Value;
    GenerateColorImage;
  finally
    fSuspendEvents := false;
  end;
end;

procedure TFSelectColor.tbChannelChange(Sender: TObject);
begin
  if fSuspendEvents then Exit;

  fSuspendEvents := true;
  try
    seAlpha.Value := tbAlpha.Position;
    seRed.Value := tbRed.Position;
    seGreen.Value := tbGreen.Position;
    seBlue.Value := tbBlue.Position;
    GenerateColorImage;
  finally
    fSuspendEvents := false;
  end;
end;

procedure TFSelectColor.GenerateColorImage;
var
  x, y: Integer;
  NewColor: TColor32;
begin
  if (imgColor.Bitmap.Width <> 40) or (imgColor.Bitmap.Height <> 40) then
    imgColor.Bitmap.SetSize(40, 40);

  NewColor := SelectedColor;

  imgColor.BeginUpdate;
  try
    for y := 0 to 39 do
      for x := 0 to 39 do
      begin
        if ((x div 4) mod 2) = ((y div 4) mod 2) then
          imgColor.Bitmap[x, y] := $FFFFFFFF
        else
          imgColor.Bitmap[x, y] := $FFCCCCCC;

        imgColor.Bitmap[x, y] := BlendReg(NewColor, imgColor.Bitmap[x, y]);
      end;
  finally
    imgColor.EndUpdate;
    imgColor.Invalidate;
  end;
end;

procedure TFSelectColor.SetSelectedColor(aValue: TColor32);
begin
  fSuspendEvents := true;
  try
    seAlpha.Value := (aValue and $FF000000) shr 24;
    seRed.Value := (aValue and $00FF0000) shr 16;
    seGreen.Value := (aValue and $0000FF00) shr 8;
    seBlue.Value := (aValue and $000000FF);

    tbAlpha.Position := seAlpha.Value;
    tbRed.Position := seRed.Value;
    tbGreen.Position := seGreen.Value;
    tbBlue.Position := seBlue.Value;

    GenerateColorImage;
  finally
    fSuspendEvents := false;
  end;
end;

function TFSelectColor.GetSelectedColor: TColor32;
begin
  Result := (seAlpha.Value shl 24) +
            (seRed.Value shl 16) +
            (seGreen.Value shl 8) +
            (seBlue.Value);
end;

end.

