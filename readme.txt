** This readme is for the source code. If you want help using the application **
** please click the ? button near the bottom-right of the application window. **

Phart is written in Lazarus 2.0.6 with compiler FPC 3.0.4. Phart development has
targeted Win32, but it will likely compile for Win64, Mac and Linux.

Compiling Phart requires Graphics32 and GR32_Png.

Phart source code is originally written by Namida Verasche ( namida.verasche@gmail.com ).
Phart is free software, but donations are welcomed via PayPal at the above email address.

Phart and its source code are made available under the MIT Licence, a copy of which is
present below.

-----

Copyright (c) 2020 Namida Verasche

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.