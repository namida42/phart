unit phart_main;

// Copyright (c) 2020 Namida Verasche
// Phart source code is made available under the MIT Licence
// See readme.txt for more information

{$mode objfpc}{$H+}

interface

uses
  palimage,
  phart_sizeform, phart_colorform, phart_about, phart_sheetform,
  Clipbrd,
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, Buttons, ExtCtrls,
  FGL, Math,
  GR32, GR32_Png, GR32_Image, GR32_Layers, GR32_OrdinalMaps, GR32_Resamplers, Types;

const
  VERSION_NUMBER = '0.91';

type
  TImage32List = specialize TFPGObjectList<TImage32>;
  TMemoryStreamList = specialize TFPGObjectList<TMemoryStream>;

  TActiveTool = (atPan, atZoom, atPanSample, atZoomSample, atPencil, atFill,
                 atPicker, atSelect, atPaste);

  TMouseDown = (mdNone, mdLeft, mdMiddle, mdRight);

  { TFPhart }

  TFPhart = class(TForm)
    imgDrawArea: TImage32;
    imgSheetManager: TImage32;
    imgQuickPalette: TImage32;
    imgZoom: TImage32;
    imgResize: TImage32;
    imgPaste: TImage32;
    imgCut: TImage32;
    imgCopy: TImage32;
    imgNew: TImage32;
    imgOpen: TImage32;
    imgHelp: TImage32;
    imgUndo: TImage32;
    imgRedo: TImage32;
    imgSave: TImage32;
    imgPanSample: TImage32;
    imgPicker: TImage32;
    imgPencil: TImage32;
    imgPalette: TImage32;
    imgPan: TImage32;
    imgFill: TImage32;
    imgLoadSample: TImage32;
    imgSelect: TImage32;
    imgZoomSample: TImage32;
    odOpenImage: TOpenDialog;
    odOpenSample: TOpenDialog;
    sdSaveImage: TSaveDialog;
    tmShowAboutFirstRun: TTimer;

    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);

    // Buttons
    procedure imgButtonMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
    procedure imgButtonMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
    procedure imgButtonMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer; Layer: TCustomLayer);

    // DrawArea
    procedure imgDrawAreaMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
    procedure imgDrawAreaMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer; Layer: TCustomLayer);
    procedure imgDrawAreaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
    procedure imgDrawAreaMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);

    // Palette
    procedure imgPaletteMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);

    procedure tmShowAboutFirstRunTimer(Sender: TObject);
  private
    fFilename: String;
    fIsSaved: Boolean;

    fSampleClonePositions: array[0..2, 0..2] of Boolean;

    fUndoHistory: TMemoryStreamList;
    fUndoPosition: Integer;
    fNeedSaveUndo: Boolean;
    fSuspendSaveUndo: Boolean;

    fButtonList: TImage32List;
    fToolButtons: array[TActiveTool] of TImage32;
    fActiveTool: TActiveTool;
    fMouseDown: TMouseDown;
    fMouseLastPanPosition: TPoint;

    fActiveButton: TImage32;
    fPaletteImage: TBitmap32;
    fSelectorImage: TBitmap32;

    fCheckerboardLayer: TBitmapLayer;
    fSampleLayer: TBitmapLayer;
    fGridLayer: TBitmapLayer;
    fImageLayer: TBitmapLayer;
    fSelectionLayer: TBitmapLayer;

    fImage: TPalImage;

    fPrimaryColorIndex: Integer;
    fSecondaryColorIndex: Integer;

    fZoom: Single;
    fOffsetX: Single;
    fOffsetY: Single;
    fSampleZoom: Single;
    fSampleOffsetX: Single;
    fSampleOffsetY: Single;

    fSelectRect: TRect;

    fSampleImage: TBitmap32;
    fSampleOpacity: Integer;

    fFirstRun: Boolean;

    procedure OnImageChanged;

    procedure LoadSettings;
    procedure SaveSettings;

    procedure PrepareToolButtons;
    procedure GeneratePaletteGraphic;
    procedure PreparePalette;
    procedure GenerateCheckerboard;
    procedure GenerateGrid;
    procedure GenerateSelection;
    procedure GenerateSample;
    procedure PositionLayers;

    procedure DrawSelectedColors;
    procedure RedrawImage(aForceRegenerate: Boolean = false);

    procedure SaveUndo;
    procedure ClearUndo;
    procedure SuspendSaveUndo;
    procedure ResumeSaveUndo(aDiscard: Boolean);

    procedure SetZoom(aZoom: Single; aPreferredDirection: Integer);
    procedure ZoomToFit;
    procedure SetSampleZoom(aZoom: Single);
    procedure SetActiveTool(aTool: TActiveTool; aRightClick: Boolean = false);

    function GetButton(aTag: Integer): TImage32;

    function GetPixelUnderMouse: TPoint;
    procedure SetSelectedColor(aIndex: Integer; aSecondary: Boolean);
    function GetPrimaryColorARGB: TColor32;
    function GetSecondaryColorARGB: TColor32;

    procedure HandleImageInput;

    procedure New;
    procedure Open;
    procedure Save;
    procedure SaveAs;
    procedure OpenSample(aRightClick: Boolean = false);

    function ConfirmSave: Boolean;

    procedure Undo;
    procedure Redo;

    procedure ResizeImage;
    procedure ClearSelection;

    procedure Cut;
    procedure Copy;
    procedure Paste(oX, oY: Integer);

    procedure Fill(oX, oY: Integer; aColorIndex: Integer);

    procedure ShowAbout;

    procedure ShowSheetManager;

    procedure DoQuickPalette(aWithTransparency: Boolean);
    procedure ResetPaletteToDefault;

    function GetAnySampleClonePositions: Boolean;

    property AnySampleClonePositions: Boolean read GetAnySampleClonePositions;
  public
    procedure LoadDirect(aStream: TStream);
    procedure SaveDirect(aStream: TStream);
    procedure LoadSampleDirect(aSrc: TBitmap32; aClonePositions: Integer);
  end;

var
  FPhart: TFPhart;

implementation

{$R *.lfm}
{$R phart_ui.rc}

{ TFPhart }

const
  MIN_ZOOM = 0.25;
  MAX_ZOOM = 128;
  GRID_MIN_ZOOM = 8;
  DEFAULT_ZOOM = 32;
  DEFAULT_SAMPLE_ZOOM = 1 / DEFAULT_ZOOM;

  UNDO_CAP = 256;

  BTN_TAG_NEW = 0;
  BTN_TAG_OPEN = 1;
  BTN_TAG_SAVE = 2;
  BTN_TAG_LOAD_SAMPLE = 3;

  BTN_TAG_SHEET_MANAGER = 4;

  BTN_TAG_UNDO = 5;
  BTN_TAG_REDO = 6;

  BTN_TAG_PAN = 7;
  BTN_TAG_ZOOM = 8;
  BTN_TAG_PAN_SAMPLE = 9;
  BTN_TAG_ZOOM_SAMPLE = 10;

  BTN_TAG_RESIZE = 11;

  BTN_TAG_PENCIL = 12;
  BTN_TAG_FILL = 13;
  BTN_TAG_PICKER = 14;

  BTN_TAG_SELECT = 15;
  BTN_TAG_CUT = 16;
  BTN_TAG_COPY = 17;
  BTN_TAG_PASTE = 18;

  BTN_TAG_QUICK_PALETTE = 19;

  BTN_TAG_HELP = 20;

procedure TFPhart.FormCreate(Sender: TObject);
begin
  Constraints.MinWidth := Width;
  Constraints.MinHeight := Height;

  fFirstRun := true;

  fImage := TPalImage.Create;
  fImage.SetSize(16, 16, paClear, 0);

  fSampleImage := TBitmap32.Create;

  fUndoHistory := TMemoryStreamList.Create;
  fButtonList := TImage32List.Create(false);
  fPaletteImage := TBitmap32.Create;
  fSelectorImage := TBitmap32.Create;

  fCheckerboardLayer := TBitmapLayer.Create(imgDrawArea.Layers);
  fSampleLayer := TBitmapLayer.Create(imgDrawArea.Layers);
  fImageLayer := TBitmapLayer.Create(imgDrawArea.Layers);   
  fGridLayer := TBitmapLayer.Create(imgDrawArea.Layers);
  fSelectionLayer := TBitmapLayer.Create(imgDrawArea.Layers);

  fCheckerboardLayer.Bitmap.DrawMode := dmBlend;
  fSampleLayer.Bitmap.DrawMode := dmBlend;
  fImageLayer.Bitmap.DrawMode := dmBlend;    
  fGridLayer.Bitmap.DrawMode := dmBlend;
  fSelectionLayer.Bitmap.DrawMode := dmBlend;

  PrepareToolButtons;
  PreparePalette;
  GenerateCheckerboard;

  fZoom := DEFAULT_ZOOM;
  fSampleZoom := DEFAULT_SAMPLE_ZOOM;

  SetActiveTool(atPencil);

  GenerateGrid;
  PositionLayers;

  fImage.OnChange := @OnImageChanged;
  fIsSaved := true;

  ClearUndo;
end;

procedure TFPhart.FormCloseQuery(Sender: TObject; var CanClose: boolean);
begin
  CanClose := ConfirmSave;
end;

procedure TFPhart.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin                                                  
  SaveSettings;

  try
    ForceDirectories(GetAppConfigDir(false));
    fImage.SavePaletteToFile(GetAppConfigDir(false) + 'phart.pal', false);
  except
    // Silently fail. This is far from critical.
  end;
end;

procedure TFPhart.FormDestroy(Sender: TObject);
begin
  fUndoHistory.Free;
  fButtonList.Free;
  fPaletteImage.Free;
  fSelectorImage.Free;
  fSampleImage.Free;

  fImage.Free;
end;

procedure TFPhart.FormResize(Sender: TObject);
begin
  imgPalette.Width := imgPalette.Height div 2;
  imgPalette.Left := ClientWidth - imgPalette.Width - 8;
  imgDrawArea.Width := ClientWidth - imgPalette.Width - 16 {side padding} - 32 {middle padding};
end;

procedure TFPhart.FormShow(Sender: TObject);
begin
  LoadSettings;

  FSheetManager.SetPaletteFrom(fImage);

  fOffsetX := (imgDrawArea.Width - (fImage.Width * fZoom)) / 2;
  fOffsetY := (imgDrawArea.Height - (fImage.Height * fZoom)) / 2;

  PositionLayers;

  if fFirstRun then
    tmShowAboutFirstRun.Enabled := true;
end;

procedure TFPhart.imgButtonMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
var
  Btn: TImage32 absolute Sender;
begin
  Btn.OffsetVert := -32 * Screen.PixelsPerInch / 96;
  fActiveButton := Btn;
end;        

procedure TFPhart.imgButtonMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
var
  Btn: TImage32 absolute Sender;
begin
  Btn.OffsetVert := 0;
  if fActiveButton <> Btn then Exit;

  fActiveButton := nil;

  case Btn.Tag of
    BTN_TAG_NEW: if Button = mbRight then
                   ResetPaletteToDefault
                 else
                   New;
    BTN_TAG_OPEN: Open;
    BTN_TAG_SAVE: if Button = mbRight then
                    SaveAs
                  else
                    Save;
    BTN_TAG_LOAD_SAMPLE: OpenSample(Button = mbRight);

    BTN_TAG_SHEET_MANAGER: ShowSheetManager;

    BTN_TAG_UNDO: Undo;
    BTN_TAG_REDO: Redo;

    BTN_TAG_PAN: SetActiveTool(atPan, Button = mbRight);
    BTN_TAG_ZOOM: SetActiveTool(atZoom, Button = mbRight);
    BTN_TAG_PAN_SAMPLE: SetActiveTool(atPanSample, Button = mbRight);
    BTN_TAG_ZOOM_SAMPLE: SetActiveTool(atZoomSample, Button = mbRight);

    BTN_TAG_RESIZE: ResizeImage;

    BTN_TAG_PENCIL: SetActiveTool(atPencil);
    BTN_TAG_FILL: SetActiveTool(atFill);
    BTN_TAG_PICKER: SetActiveTool(atPicker);

    BTN_TAG_SELECT: SetActiveTool(atSelect, Button = mbRight);
    BTN_TAG_CUT: Cut;
    BTN_TAG_COPY: Copy;
    BTN_TAG_PASTE: SetActiveTool(atPaste);

    BTN_TAG_QUICK_PALETTE: DoQuickPalette(Button = mbRight);

    BTN_TAG_HELP: ShowAbout;
  end;
end;

procedure TFPhart.imgButtonMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer; Layer: TCustomLayer);
var
  Btn: TImage32 absolute Sender;
begin
  if fActiveButton <> Btn then Exit;
  if not PtInRect(Rect(0, 0, 32, 32), Point(X, Y)) then
  begin
    Btn.OffsetVert := 0;
    fActiveButton := nil;
  end;
end;

procedure TFPhart.imgDrawAreaMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
begin
  case Button of
    mbLeft: fMouseDown := mdLeft;
    mbMiddle: fMouseDown := mdMiddle;
    mbRight: fMouseDown := mdRight;
  end;

  if (fActiveTool = atPan) or (fActiveTool = atPanSample) or (Button = mbMiddle) then
    fMouseLastPanPosition := Mouse.CursorPos;

  SuspendSaveUndo;

  HandleImageInput;
end;

procedure TFPhart.imgDrawAreaMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer; Layer: TCustomLayer);
begin
  if not PtInRect(imgDrawArea.BoundsRect, ScreenToClient(Mouse.CursorPos)) then
  begin
    fMouseDown := mdNone;
    ResumeSaveUndo(false);
  end;

  if fMouseDown <> mdNone then
    HandleImageInput;
end;

procedure TFPhart.imgDrawAreaMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
begin      
  fMouseDown := mdNone;
  ResumeSaveUndo(false);
end;

procedure TFPhart.imgDrawAreaMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
  if WheelDelta > 1 then
    SetZoom(fZoom, 2)
  else
    SetZoom(fZoom, -2);
end;

procedure TFPhart.imgPaletteMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
var
  IsSecondary: Boolean;
  ImgPoint: TPoint;
  NewIndex: Integer;
begin
  case Button of
    mbLeft: IsSecondary := false;
    mbRight: IsSecondary := true;
    else Exit;
  end;

  ImgPoint := imgPalette.ControlToBitmap(Point(X, Y));
  NewIndex := (ImgPoint.X div 32) + ((ImgPoint.Y div 32) * 8);

  if ((NewIndex = fPrimaryColorIndex) and not IsSecondary) or
     ((NewIndex = fSecondaryColorIndex) and IsSecondary) then
  begin
    FSelectColor.SelectedColor := fImage.Palette[NewIndex];
    if FSelectColor.ShowModal = mrOk then
    begin
      fImage.Palette[NewIndex] := FSelectColor.SelectedColor;

      GeneratePaletteGraphic;
      DrawSelectedColors;
      RedrawImage;
    end;
  end else
    SetSelectedColor(NewIndex, IsSecondary);
end;

procedure TFPhart.tmShowAboutFirstRunTimer(Sender: TObject);
begin
  tmShowAboutFirstRun.Enabled := false;
  ShowAbout;
end;

function TFPhart.GetAnySampleClonePositions: Boolean;
var
  x, y: Integer;
begin
  Result := false;
  for y := 0 to 2 do
    for x := 0 to 2 do
      if fSampleClonePositions[x, y] then
      begin
        Result := true;
        Exit;
      end;
end;

procedure TFPhart.OnImageChanged;
begin
  if fIsSaved then
  begin
    if fFilename = '' then
      Caption := 'Phart *'
    else
      Caption := 'Phart - ' + ExtractFileName(fFilename) + ' *';
  end;
  fIsSaved := false;
  SaveUndo;
  RedrawImage;
end;

procedure TFPhart.LoadSettings;
var
  SL: TStringList;
begin
  if FileExists(GetAppConfigDir(false) + 'phart.ini') then
  begin
    SL := TStringList.Create;
    try
      SL.LoadFromFile(GetAppConfigDir(false) + 'phart.ini');
    finally
      SL.Free;
    end;

    fFirstRun := false;
  end;
end;

procedure TFPhart.SaveSettings;
var
  SL: TStringList;
begin
  SL := TStringList.Create;
  try

    try
      ForceDirectories(GetAppConfigDir(false));
      SL.SaveToFile(GetAppConfigDir(false) + 'phart.ini');
    except
      // Silent fail.
    end;
  finally
    SL.Free;
  end;
end;

procedure TFPhart.PrepareToolButtons;
var
  SrcBitmap: TBitmap32;
  R: TResourceStream;

  procedure SetupButton(aTarget: TImage32; aImageIndex: Integer; aTag: Integer);
  var
    BMP: TBitmap32;
    SrcRect: TRect;
  begin
    BMP := aTarget.Bitmap;
    BMP.SetSize(32, 64);

    SrcBitmap.DrawTo(BMP, 0, 0, Rect(0, 0, 32, 32));
    SrcBitmap.DrawTo(BMP, 0, 32, Rect(32, 0, 64, 32));

    SrcRect := Rect((aImageIndex mod 10) * 32, (aImageIndex div 10) * 32, 0, 0);
    SrcRect.Right := SrcRect.Left + 32;
    SrcRect.Bottom := SrcRect.Top + 32;

    SrcBitmap.DrawTo(BMP, 0, 0, SrcRect);
    SrcBitmap.DrawTo(BMP, 0, 32, SrcRect);

    aTarget.Tag := aTag;
    aTarget.Scale := Screen.PixelsPerInch / 96;
    if Screen.PixelsPerInch mod 96 <> 0 then
      TLinearResampler.Create(BMP);

    fButtonList.Add(aTarget);
  end;
begin
  SrcBitmap := TBitmap32.Create;
  try
    R := TResourceStream.Create(HInstance, 'UI', 'BUTTONS');
    try
      LoadBitmap32FromPng(SrcBitmap, R);
    finally
      R.Free;
    end;

    SrcBitmap.DrawMode := dmBlend;

    SetupButton(imgNew, 2, BTN_TAG_NEW);
    SetupButton(imgOpen, 3, BTN_TAG_OPEN);
    SetupButton(imgSave, 4, BTN_TAG_SAVE);
    SetupButton(imgLoadSample, 6, BTN_TAG_LOAD_SAMPLE);

    SetupButton(imgSheetManager, 25, BTN_TAG_SHEET_MANAGER);

    SetupButton(imgUndo, 7, BTN_TAG_UNDO);
    SetupButton(imgRedo, 8, BTN_TAG_REDO);

    SetupButton(imgPan, 9, BTN_TAG_PAN);
    SetupButton(imgZoom, 10, BTN_TAG_ZOOM);
    SetupButton(imgPanSample, 11, BTN_TAG_PAN_SAMPLE);
    SetupButton(imgZoomSample, 12, BTN_TAG_ZOOM_SAMPLE);

    SetupButton(imgResize, 13, BTN_TAG_RESIZE);

    SetupButton(imgPencil, 14, BTN_TAG_PENCIL);
    SetupButton(imgFill, 15, BTN_TAG_FILL);
    SetupButton(imgPicker, 16, BTN_TAG_PICKER);

    SetupButton(imgSelect, 17, BTN_TAG_SELECT);
    SetupButton(imgCut, 19, BTN_TAG_CUT);
    SetupButton(imgCopy, 20, BTN_TAG_COPY);
    SetupButton(imgPaste, 21, BTN_TAG_PASTE);

    SetupButton(imgQuickPalette, 18, BTN_TAG_QUICK_PALETTE);

    SetupButton(imgHelp, 22, BTN_TAG_HELP);

    fSelectorImage.SetSize(64, 32);
    fSelectorImage.Clear($00000000);
    SrcBitmap.DrawTo(fSelectorImage, 0, 0, Rect(96, 64, 160, 96));
    fSelectorImage.DrawMode := dmBlend;

    fToolButtons[atPan] := imgPan;
    fToolButtons[atZoom] := imgZoom;
    fToolButtons[atPanSample] := imgPanSample;
    fToolButtons[atZoomSample] := imgZoomSample;
    fToolButtons[atPencil] := imgPencil;
    fToolButtons[atFill] := imgFill;
    fToolButtons[atPicker] := imgPicker;
    fToolButtons[atSelect] := imgSelect;
    fToolButtons[atPaste] := imgPaste;
  finally
    SrcBitmap.Free;
  end;
end;

procedure TFPhart.GeneratePaletteGraphic;
var
  BMP, OverlayBMP: TBitmap32;
  x, y, i: Integer;
  DstRect: TRect;
begin
  BMP := fPaletteImage;
  OverlayBMP := TBitmap32.Create;
  try
    BMP.SetSize(256, 512);
    OverlayBMP.SetSize(256, 512);

    // Checkerboard background
    for y := 0 to 511 do
      for x := 0 to 255 do
        if ((x div 4) mod 2) = ((y div 4) mod 2) then
          BMP[x, y] := $FFFFFFFF
        else
          BMP[x, y] := $FFCCCCCC;

    // Palette graphic
    for i := 0 to 127 do
    begin
      DstRect.Left := (i mod 8) * 32;
      DstRect.Top := (i div 8) * 32;
      DstRect.Right := DstRect.Left + 32;
      DstRect.Bottom := DstRect.Top + 32;
      OverlayBMP.FillRect(DstRect.Left, DstRect.Top, DstRect.Right, DstRect.Bottom, fImage.Palette[i]);
    end;

    OverlayBMP.DrawMode := dmBlend;
    OverlayBMP.DrawTo(BMP);

    fPaletteImage.Assign(BMP);
  finally
    OverlayBMP.Free;
  end;
end;

procedure TFPhart.PreparePalette;
  procedure LoadPaletteFromFile;
  begin
    fImage.LoadPaletteFromFile(GetAppConfigDir(false) + 'phart.pal', false);
  end;

  procedure SetupDefaultPalette;
  begin
    fImage.SetPaletteToDefault;
  end;
var
  LoadedFromFile: Boolean;
begin
  LoadedFromFile := false;

  if FileExists(GetAppConfigDir(false) + 'phart.pal') then
  begin
    try
      LoadPaletteFromFile;
      LoadedFromFile := true;
    except
      ShowMessage('Loading palette from file failed. Using default palette.');
    end;
  end;

  if not LoadedFromFile then
    SetupDefaultPalette;

  GeneratePaletteGraphic;

  fSecondaryColorIndex := 0;
  SetSelectedColor(7, false); // Mostly to force drawing the palette graphic
end;

procedure TFPhart.GenerateCheckerboard;
var
  x, y: Integer;
begin
  fCheckerboardLayer.BeginUpdate;
  try
    fCheckerboardLayer.Bitmap.SetSize(fImage.Width * 2, fImage.Height * 2);
    for y := 0 to fCheckerboardLayer.Bitmap.Height-1 do
      for x := 0 to fCheckerboardLayer.Bitmap.Width-1 do
        if (x mod 2) = (y mod 2) then
          fCheckerboardLayer.Bitmap[x, y] := $FFFFFFFF
        else
          fCheckerboardLayer.Bitmap[x, y] := $FFCCCCCC;
  finally
    fCheckerboardLayer.Bitmap.EndUpdate;
    imgDrawArea.Invalidate;
  end;
end;

procedure TFPhart.GenerateGrid;
var
  x, y: Integer;
  LocalZoom: Integer;
begin
  LocalZoom := Round(fZoom);

  fGridLayer.BeginUpdate;
  try
    fGridLayer.Bitmap.SetSize(fImage.Width * LocalZoom, fImage.Height * LocalZoom);
    fGridLayer.Bitmap.Clear($00000000);

    if fZoom < GRID_MIN_ZOOM then Exit;

    for y := 0 to fImage.Height-1 do
      for x := 0 to fImage.Width-1 do
        fGridLayer.Bitmap.FrameRectS(x * LocalZoom, y * LocalZoom, (x + 1) * LocalZoom, (y + 1) * LocalZoom, $FF000000);
  finally
    fGridLayer.EndUpdate;
  end;
end;

procedure TFPhart.GenerateSelection;
var
  BMP: TBitmap32;
begin
  if (fSelectRect.Width <= 0) or (fSelectRect.Height <= 0) then Exit;
  BMP := fSelectionLayer.Bitmap;

  fSelectionLayer.BeginUpdate;
  try
    BMP.SetSize(Round(fSelectRect.Width * fZoom) + 2, Round(fSelectRect.Height * fZoom) + 2);

    BMP.FrameRectS(0, 0, BMP.Width, BMP.Height, $FFFF00FF);
    BMP.FrameRectS(1, 1, BMP.Width-1, BMP.Height-1, $FFCC00CC);

    if fZoom >= 16 then
    begin
      BMP.FrameRectS(2, 2, BMP.Width-2, BMP.Height-2, $FFFF00FF);
      BMP.FrameRectS(3, 3, BMP.Width-3, BMP.Height-3, $FFCC00CC);
    end;
  finally
    fSelectionLayer.EndUpdate;
  end;
end;

procedure TFPhart.GenerateSample;
var
  x, y: Integer;
  A, R, G, B: Byte;
begin
  if fSampleOpacity = 0 then
  begin
    fSampleLayer.Bitmap.Clear(0);
    Exit;
  end;

  if (fSampleLayer.Index > fImageLayer.Index) and (fSampleOpacity > 3) then
    fSampleOpacity := 3;

  if AnySampleClonePositions then
  begin
    fImageLayer.Bitmap.DrawMode := dmOpaque;

    for y := 0 to 2 do
      for x := 0 to 2 do
        if fSampleClonePositions[x, y] then
          fImageLayer.Bitmap.DrawTo(fSampleImage, x * fSampleImage.Width div 3, y * fSampleImage.Height div 3);

    fImageLayer.Bitmap.DrawMode := dmBlend;
  end;

  fSampleLayer.BeginUpdate;
  try
    fSampleLayer.Bitmap.SetSize(fSampleImage.Width, fSampleImage.Height);
    for y := 0 to fSampleImage.Height-1 do
      for x := 0 to fSampleImage.Width-1 do
      begin
        Color32ToRGBA(fSampleImage[x, y], R, G, B, A);
        A := (A * fSampleOpacity) div 4;
        fSampleLayer.Bitmap[x, y] := Color32(R, G, B, A);
      end;
  finally
    fSampleLayer.EndUpdate;
    fSampleLayer.Changed;
  end;
end;

procedure TFPhart.PositionLayers; 
var
  SampleRect: TFloatRect;
  SelectRect: TFloatRect;
begin
  fCheckerboardLayer.Location := FloatRect(fOffsetX, fOffsetY, fOffsetX + (fImage.Width * fZoom), fOffsetY + (fImage.Height * fZoom));

  SampleRect.Left := fOffsetX + fSampleOffsetX;
  SampleRect.Top := fOffsetY + fSampleOffsetY;
  SampleRect.Right := SampleRect.Left + (fSampleLayer.Bitmap.Width * fZoom * fSampleZoom);
  SampleRect.Bottom := SampleRect.Top + (fSampleLayer.Bitmap.Height * fZoom * fSampleZoom);

  fSampleLayer.Location := SampleRect;

  fGridLayer.Location := fCheckerboardLayer.Location;
  fImageLayer.Location := fCheckerboardLayer.Location;

  if (fSelectRect.Width > 0) and (fSelectRect.Height > 0) then
  begin
    SelectRect.Left := fImageLayer.Location.Left + (fSelectRect.Left * fZoom) - 1;
    SelectRect.Top := fImageLayer.Location.Top + (fSelectRect.Top * fZoom) - 1;
    SelectRect.Right := SelectRect.Left + (fSelectRect.Width * fZoom) + 2;
    SelectRect.Bottom := SelectRect.Top + (fSelectRect.Height * fZoom) + 2;

    fSelectionLayer.Visible := true;
    fSelectionLayer.Location := SelectRect;
  end else
    fSelectionLayer.Visible := false;
end;

procedure TFPhart.DrawSelectedColors;
begin
  imgPalette.BeginUpdate;
  try
    imgPalette.Bitmap.Assign(fPaletteImage);
    fSelectorImage.DrawTo(imgPalette.Bitmap, (fSecondaryColorIndex mod 8) * 32, (fSecondaryColorIndex div 8) * 32,
                          Rect(32, 0, 64, 32));
    fSelectorImage.DrawTo(imgPalette.Bitmap, (fPrimaryColorIndex mod 8) * 32, (fPrimaryColorIndex div 8) * 32,
                          Rect(0, 0, 32, 32));
  finally
    imgPalette.EndUpdate;
    imgPalette.Invalidate;
  end;
end;

procedure TFPhart.RedrawImage(aForceRegenerate: Boolean = false);
begin
  fImage.WriteBitmap32(fImageLayer.Bitmap);
  if (fImage.Width * fZoom <> (fImageLayer.Location.Right - fImageLayer.Location.Left)) or
     (fImage.Height * fZoom <> (fImageLayer.Location.Bottom - fImageLayer.Location.Top)) or
     aForceRegenerate then
  begin
    GenerateCheckerboard;
    GenerateGrid;
    PositionLayers;
  end;

  imgDrawArea.Invalidate;

  if AnySampleClonePositions then
    GenerateSample;
end;

procedure TFPhart.SaveUndo;
var
  MS: TMemoryStream;
begin
  if fSuspendSaveUndo then
    fNeedSaveUndo := true
  else begin
    fNeedSaveUndo := false;
    while fUndoPosition < fUndoHistory.Count do
      fUndoHistory.Delete(fUndoPosition);
    MS := TMemoryStream.Create;
    fImage.SaveToStream(MS);
    fUndoHistory.Add(MS);
    Inc(fUndoPosition);

    while fUndoHistory.Count > UNDO_CAP do
    begin
      fUndoHistory.Delete(0);
      Dec(fUndoPosition);
    end;
  end;
end;

procedure TFPhart.ClearUndo;
begin
  fUndoHistory.Clear;
  fUndoPosition := 0;
  SaveUndo;
end;

procedure TFPhart.SuspendSaveUndo;
begin
  fSuspendSaveUndo := true;
end;

procedure TFPhart.ResumeSaveUndo(aDiscard: Boolean);
begin
  fSuspendSaveUndo := false;
  if aDiscard then
    fNeedSaveUndo := false;
  if fNeedSaveUndo then
    SaveUndo;
end;

procedure TFPhart.SetZoom(aZoom: Single; aPreferredDirection: Integer);
var
  LowCandidate, HighCandidate: Single;

  function DoneDownwards: Boolean;
  begin
    case aPreferredDirection of
      -2: Result := LowCandidate < aZoom;
      -1: Result := LowCandidate <= aZoom;
       1: Result := LowCandidate < aZoom;
       2: Result := LowCandidate <= aZoom;
      else Result := true;
    end;
  end;

  function DoneUpwards: Boolean;
  begin
    case aPreferredDirection of
      -2: Result := HighCandidate >= aZoom;
      -1: Result := HighCandidate > aZoom;
       1: Result := HighCandidate >= aZoom;
       2: Result := HighCandidate > aZoom;
      else Result := true;
    end;
  end;

begin
  aZoom := Min(Max(MIN_ZOOM, aZoom), MAX_ZOOM);

  if (aZoom = 1) and (Abs(aPreferredDirection) = 2) then
  begin
    if aPreferredDirection < 0 then
      aZoom := 1 / 2
    else
      aZoom := 1 + 0.5;
  end else begin
    LowCandidate := 1;
    HighCandidate := 1;

    if aPreferredDirection = 0 then
    begin
      if aZoom < 1 then
        aPreferredDirection := 1
      else
        aPreferredDirection := -1;
    end;

    if aZoom < 1 then
    begin
      repeat
        HighCandidate := LowCandidate;
        LowCandidate := HighCandidate / 2;
      until DoneDownwards;
    end else if aZoom > 1 then
    begin
      repeat
        LowCandidate := HighCandidate;
        if LowCandidate < 3 then
          HighCandidate := LowCandidate + 0.5
        else if LowCandidate < 8 then
          HighCandidate := LowCandidate + 1
        else if LowCandidate < 32 then
          HighCandidate := LowCandidate + 2
        else
          HighCandidate := LowCandidate + 4;
      until DoneUpwards;
    end;

    if (aPreferredDirection < 0) then
      aZoom := LowCandidate
    else
      aZoom := HighCandidate;
  end;

  aZoom := Min(Max(MIN_ZOOM, aZoom), MAX_ZOOM);

  fOffsetX := ((fOffsetX - (imgDrawArea.Width / 2)) / fZoom * aZoom) + (imgDrawArea.Width / 2);
  fOffsetY := ((fOffsetY - (imgDrawArea.Height / 2)) / fZoom * aZoom) + (imgDrawArea.Height / 2); 
  fSampleOffsetX := fSampleOffsetX / fZoom * aZoom;
  fSampleOffsetY := fSampleOffsetY / fZoom * aZoom;
  fZoom := aZoom;

  GenerateGrid;
  GenerateSelection;
  PositionLayers;
end;

procedure TFPhart.ZoomToFit;
var
  TargetZoom: Single;
begin
  // Just in case.
  if (fImage.Width = 0) or (fImage.Height = 0) then Exit;

  TargetZoom := Min(imgDrawArea.Width / fImage.Width, imgDrawArea.Height / fImage.Height);
  SetZoom(TargetZoom, -1);
end;

procedure TFPhart.SetSampleZoom(aZoom: Single);
begin
  aZoom := Min(Max(1 / MAX_ZOOM, aZoom), MAX_ZOOM);

  fSampleOffsetX := ((fSampleOffsetX - (imgDrawArea.Width / 2)) / fSampleZoom * aZoom) + (imgDrawArea.Width / 2);
  fSampleOffsetY := ((fSampleOffsetY - (imgDrawArea.Height / 2)) / fSampleZoom * aZoom) + (imgDrawArea.Height / 2);
  fSampleZoom := aZoom;

  PositionLayers;
end;

procedure TFPhart.SetActiveTool(aTool: TActiveTool; aRightClick: Boolean);
  procedure SetSampleMaxZoom;
  var
    ExactMaxZoom: Single;
    NewSampleZoom: Single;
  begin
    ExactMaxZoom := Min(fImage.Width / fSampleLayer.Bitmap.Width,
                        fImage.Height / fSampleLayer.Bitmap.Height);
    NewSampleZoom := 1;

    if ExactMaxZoom < 1 then
    begin
      while NewSampleZoom > ExactMaxZoom do
        NewSampleZoom := NewSampleZoom / 2;
    end else if ExactMaxZoom > 1 then
    begin
      while NewSampleZoom * 2 < ExactMaxZoom do
        NewSampleZoom := NewSampleZoom * 2;
    end;

    SetSampleZoom(NewSampleZoom);
  end;

begin
  fToolButtons[fActiveTool].OffsetVert := 0;
  fActiveTool := aTool;
  fToolButtons[aTool].OffsetVert := -32 * Screen.PixelsPerInch / 96;

  if aRightClick then
  begin
    case aTool of
      atPan: begin
               fOffsetX := (imgDrawArea.Width - (fImage.Width * fZoom)) / 2;
               fOffsetY := (imgDrawArea.Height - (fImage.Height * fZoom)) / 2;
               PositionLayers;
             end;
      atZoom: ZoomToFit;

      atPanSample: begin
                     fSampleOffsetX := Round(fSampleOffsetX / fZoom) * fZoom;
                     fSampleOffsetY := Round(fSampleOffsetY / fZoom) * fZoom;
                     PositionLayers;
                   end;
      atZoomSample: SetSampleMaxZoom;
      atSelect: ClearSelection;
    end;
  end;
end;

function TFPhart.GetButton(aTag: Integer): TImage32;
begin
  for Result in fButtonList do
    if Result.Tag = aTag then Exit;
  raise Exception.Create('No matching button found for tag.');
end;

function TFPhart.GetPixelUnderMouse: TPoint;
begin
  Result := imgDrawArea.ScreenToClient(Mouse.CursorPos);
  Result.X := Trunc((Result.X - fOffsetX) / fZoom);
  Result.Y := Trunc((Result.Y - fOffsetY) / fZoom);
end;

procedure TFPhart.SetSelectedColor(aIndex: Integer; aSecondary: Boolean);
begin
  if aSecondary then
    fSecondaryColorIndex := aIndex
  else
    fPrimaryColorIndex := aIndex;

  DrawSelectedColors;
end;

function TFPhart.GetPrimaryColorARGB: TColor32;
begin
  Result := fImage.Palette[fPrimaryColorIndex];
end;

function TFPhart.GetSecondaryColorARGB: TColor32;
begin
  Result := fImage.Palette[fSecondaryColorIndex];
end;

procedure TFPhart.HandleImageInput;
var
  LocalAction: TActiveTool;
  PixelPoint: TPoint;

  PrevSelectionRect: TRect;

  function IsValidPixelPoint: Boolean;
  begin
    Result := (PixelPoint.X >= 0) and
              (PixelPoint.X < fImage.Width) and
              (PixelPoint.Y >= 0) and
              (PixelPoint.Y < fImage.Height);
  end;

begin
  if (fMouseDown = mdMiddle) then
    LocalAction := atPan
  else
    LocalAction := fActiveTool;

  PixelPoint := GetPixelUnderMouse;

  case LocalAction of
    atPan: begin
             fOffsetX := fOffsetX + (Mouse.CursorPos.X - fMouseLastPanPosition.X);
             fOffsetY := fOffsetY + (Mouse.CursorPos.Y - fMouseLastPanPosition.Y);
             fMouseLastPanPosition := Mouse.CursorPos;
             PositionLayers;
           end;
    atZoom: begin
              if fMouseDown = mdLeft then
                SetZoom(fZoom, 2)
              else
                SetZoom(fZoom, -2);
              fMouseDown := mdNone;
            end;  
    atPanSample: begin
                   fSampleOffsetX := fSampleOffsetX + (Mouse.CursorPos.X - fMouseLastPanPosition.X);
                   fSampleOffsetY := fSampleOffsetY + (Mouse.CursorPos.Y - fMouseLastPanPosition.Y);
                   fMouseLastPanPosition := Mouse.CursorPos;
                   PositionLayers;
                 end;
    atZoomSample: begin
                    if fMouseDown = mdLeft then
                      SetSampleZoom(fSampleZoom * 2)
                    else
                      SetSampleZoom(fSampleZoom / 2);
                    fMouseDown := mdNone;
                  end;

    atPencil: if IsValidPixelPoint then
              begin
                if (fSelectRect.Width = 0) or (fSelectRect.Height = 0) or
                   PtInRect(fSelectRect, PixelPoint) then
                begin // for clarity
                  if fMouseDown = mdLeft then
                    fImage.Pixel[PixelPoint.X, PixelPoint.Y] := fPrimaryColorIndex
                  else
                    fImage.Pixel[PixelPoint.X, PixelPoint.Y] := fSecondaryColorIndex;
                end;
              end;
    atFill: if IsValidPixelPoint then
            begin
              if fMouseDown = mdLeft then
                Fill(PixelPoint.X, PixelPoint.Y, fPrimaryColorIndex)
              else
                Fill(PixelPoint.X, PixelPoint.Y, fSecondaryColorIndex);

              fMouseDown := mdNone;
            end;
    atPicker: if IsValidPixelPoint then
              begin
                if fMouseDown = mdLeft then
                  SetSelectedColor(fImage.Pixel[PixelPoint.X, PixelPoint.Y], false)
                else
                  SetSelectedColor(fImage.Pixel[PixelPoint.X, PixelPoint.Y], true);

                fMouseDown := mdNone;
              end;
    atSelect: if IsValidPixelPoint then
              begin
                PrevSelectionRect := fSelectRect;

                if fMouseDown = mdLeft then
                begin        
                  fSelectRect.BottomRight := Point(PixelPoint.X + 1, PixelPoint.Y + 1);
                  if (PrevSelectionRect.Width < 1) or (fSelectRect.Width < 1) then fSelectRect.Left := fSelectRect.Right - 1;
                  if (PrevSelectionRect.Height < 1) or (fSelectRect.Height < 1) then fSelectRect.Top := fSelectRect.Bottom - 1;
                end else begin
                  fSelectRect.TopLeft := PixelPoint;
                  if (PrevSelectionRect.Width < 1) or (fSelectRect.Width < 1) then fSelectRect.Right := fSelectRect.Left + 1;
                  if (PrevSelectionRect.Height < 1) or (fSelectRect.Height < 1) then fSelectRect.Bottom := fSelectRect.Top + 1;
                end;

                if PrevSelectionRect <> fSelectRect then
                begin
                  GenerateSelection;
                  PositionLayers;
                end;
              end;
    atPaste: begin
               if fMouseDown = mdLeft then
                 Paste(PixelPoint.X, PixelPoint.Y)
               else
                 Undo;

               fMouseDown := mdNone;
             end;
  end;
end;

procedure TFPhart.New;
begin
  if FImageSize.ShowNew = mrOK then
  begin
    if not ConfirmSave then Exit;

    Caption := 'Phart';

    fFilename := '';
    fIsSaved := true;

    fImage.SetSize(FImageSize.WidthValue, FImageSize.HeightValue, paClear, fSecondaryColorIndex);
    ClearUndo;
    ClearSelection;

    RedrawImage(true);
  end;
end;

procedure TFPhart.Open;
var
  TempBMP: TBitmap32;
begin
  if odOpenImage.Execute then
  begin
    if (odOpenImage.FilterIndex < 4) and not ConfirmSave then Exit;

    case odOpenImage.FilterIndex of
      1: begin {PNG}
           TempBMP := TBitmap32.Create;
           try
             LoadBitmap32FromPng(TempBMP, odOpenImage.FileName);
             fImage.ReadBitmap32(TempBMP);
           finally
             TempBMP.Free;
           end;
         end;
      2: begin {BMP}
           TempBMP := TBitmap32.Create;
           try
             TempBMP.LoadFromFile(odOpenImage.FileName);
             fImage.ReadBitmap32(TempBMP);
           finally
             TempBMP.Free;
           end;
         end;
      3: fImage.LoadFromFile(odOpenImage.FileName); {PHT}
      4: fImage.LoadPaletteFromFile(odOpenImage.FileName, true); {TXT}
      5: fImage.LoadPaletteFromFile(odOpenImage.FileName, false); {PAL}
    end;

    if odOpenImage.FilterIndex < 4 then
    begin
      fFilename := odOpenImage.FileName;
      fIsSaved := true;
      Caption := 'Phart - ' + ExtractFileName(fFilename);
      ClearUndo;
      ClearSelection;
    end else begin
      GeneratePaletteGraphic;
      DrawSelectedColors;
    end;

    RedrawImage(true);
  end;
end;

procedure TFPhart.Save;
var
  Ext: String;
  TempBMP: TBitmap32;
begin
  if fFilename = '' then
    SaveAs
  else begin
    Ext := Uppercase(ExtractFileExt(fFilename));
    if Ext = '.PHT' then
      fImage.SaveToFile(fFilename)
    else begin
      TempBMP := TBitmap32.Create;
      try
        fImage.WriteBitmap32(TempBMP);
        if Ext = '.BMP' then
          TempBMP.SaveToFile(fFilename)
        else
          SaveBitmap32ToPNG(TempBMP, fFilename);
      finally
        TempBMP.Free;
      end;
    end;

    fIsSaved := true;
    Caption := 'Phart - ' + ExtractFileName(fFilename);
  end;
end;

procedure TFPhart.SaveAs;
begin
  if sdSaveImage.Execute then
  begin
    case sdSaveImage.FilterIndex of
      1..3: begin {PNG, BMP, PHT}
              fFilename := sdSaveImage.FileName;
              Save;
            end;
      4: fImage.SavePaletteToFile(sdSaveImage.FileName, true);
      5: fImage.SavePaletteToFile(sdSaveImage.FileName, false);
    end;
  end;
end;

procedure TFPhart.OpenSample(aRightClick: Boolean);
begin
  if aRightClick then
  begin
    Dec(fSampleOpacity);
    if fSampleOpacity < 0 then
    begin
      fSampleOpacity := 4;
      if fSampleLayer.Index > fImageLayer.Index then
        fSampleLayer.Index := fImageLayer.Index
      else
        fSampleLayer.Index := fImageLayer.Index + 1;
    end;
  end else begin
    if odOpenSample.Execute then
    begin
      case odOpenSample.FilterIndex of
        0: LoadBitmap32FromPng(fSampleImage, odOpenSample.FileName); {PNG}
        1: fSampleImage.LoadFromFile(odOpenSample.FileName); {BMP}
      end;

      if fSampleOpacity = 0 then
      begin
        fSampleOpacity := 4;
        if fSampleLayer.Index > fImageLayer.Index then
          fSampleLayer.Index := fImageLayer.Index;
      end;

      FillChar(fSampleClonePositions[0], SizeOf(fSampleClonePositions), 0);
    end;
  end;

  GenerateSample;
  PositionLayers;
end;

function TFPhart.ConfirmSave: Boolean;
begin
  if fIsSaved then
    Result := true
  else case MessageDlg('Save?', 'Do you want to save?', mtCustom, [mbYes, mbNo, mbCancel], 0) of
    mrYes: if fFilename <> '' then
           begin
             Save;
             Result := true;
           end else begin
             SaveAs;
             Result := fFilename <> '';
           end;
    mrNo: Result := true;
    else {mrCancel} Result := false;
  end;
end;

procedure TFPhart.Undo;
var
  MS: TMemoryStream;
begin
  if fUndoPosition > 1 then
  begin
    Dec(fUndoPosition);

    MS := fUndoHistory[fUndoPosition - 1];
    MS.Position := 0;

    SuspendSaveUndo;
    fImage.LoadFromStream(MS);
    ResumeSaveUndo(true);

    GeneratePaletteGraphic;
    DrawSelectedColors;

    RedrawImage(true);
  end;
end;

procedure TFPhart.Redo;
var
  MS: TMemoryStream;
begin
  if fUndoPosition < fUndoHistory.Count then
  begin
    Inc(fUndoPosition);

    MS := fUndoHistory[fUndoPosition - 1];
    MS.Position := 0;

    SuspendSaveUndo;
    fImage.LoadFromStream(MS);
    ResumeSaveUndo(true);
                       
    GeneratePaletteGraphic;
    DrawSelectedColors;

    RedrawImage(true);
  end;
end;

procedure TFPhart.ResizeImage;
begin
  if FImageSize.ShowResize(fImage.Width, fImage.Height) = mrOK then
  begin
    fImage.SetSize(FImageSize.WidthValue, FImageSize.HeightValue, TPreserveAnchor(FImageSize.AnchorValue + 1), fSecondaryColorIndex);

    ClearSelection;
    RedrawImage(true);
  end;
end;

procedure TFPhart.ClearSelection;
begin
  fSelectRect := Rect(0, 0, 0, 0);
  PositionLayers;
end;

procedure TFPhart.Cut;
var
  x, y: Integer;
begin
  if (fSelectRect.Width = 0) or (fSelectRect.Height = 0) then Exit;
  Copy;

  SuspendSaveUndo;

  for y := fSelectRect.Top to fSelectRect.Bottom-1 do
    for x := fSelectRect.Left to fSelectRect.Right-1 do
      fImage.Pixel[x, y] := fSecondaryColorIndex;

  ResumeSaveUndo(false);
  ClearSelection;
end;

procedure TFPhart.Copy;
var
  CopyString: String;
  x, y: Integer;
begin
  if (fSelectRect.Width = 0) or (fSelectRect.Height = 0) then Exit;

  CopyString := '$PHART-IMAGE-DATA|' + IntToStr(fSelectRect.Width) + '|' + IntToStr(fSelectRect.Height);

  for y := fSelectRect.Top to fSelectRect.Bottom-1 do
    for x := fSelectRect.Left to fSelectRect.Right-1 do
      CopyString := CopyString + '|' + IntToStr(fImage.Pixel[x, y]);

  Clipboard.AsText := CopyString;
end;

procedure TFPhart.Paste(oX, oY: Integer);
var
  X, Y: Integer;
  TempMap: TByteMap;
  SL: TStringList;
begin
  SL := TStringList.Create;
  TempMap := TByteMap.Create;
  SuspendSaveUndo;
  try
    SL.Delimiter := '|';
    SL.StrictDelimiter := true;
    SL.DelimitedText := Clipboard.AsText;

    if (SL.Count = 0) or (SL[0] <> '$PHART-IMAGE-DATA') then Exit;

    try
      SL.Delete(0);
      X := StrToInt(SL[0]);
      Y := StrToInt(SL[1]);
      TempMap.SetSize(X, Y);
      SL.Delete(0);
      SL.Delete(0);

      for Y := 0 to TempMap.Height-1 do
        for X := 0 to TempMap.Width-1 do
        begin
          TempMap[X, Y] := StrToInt(SL[0]);
          SL.Delete(0);
        end;

      for Y := 0 to TempMap.Height-1 do
      begin
        if (Y + oY < 0) or (Y + oY >= fImage.Height) then Continue;

        for X := 0 to TempMap.Width-1 do
        begin
          if (X + oX < 0) or (X + oX >= fImage.Width) then Continue;
          if (fImage.Palette[TempMap[X, Y]] and $FF000000) = 0 then Continue;
          fImage.Pixel[X + oX, Y + oY] := TempMap[X, Y];
        end;
      end;
    except
      // Silent fail. It's invalid clipboard data; no reason to crash.
    end;
  finally
    SL.Free;
    TempMap.Free;   
    ResumeSaveUndo(false);
  end;
end;

procedure TFPhart.Fill(oX, oY: Integer; aColorIndex: Integer);
var
  SourceColor: Integer;
  LimitRect: TRect;

  procedure RecursiveFill(X, Y: Integer);
  begin
    if not PtInRect(LimitRect, Point(X, Y)) then Exit;
    if fImage.Pixel[X, Y] <> SourceColor then Exit;

    fImage.Pixel[X, Y] := aColorIndex;

    RecursiveFill(X - 1, Y);
    RecursiveFill(X + 1, Y);
    RecursiveFill(X, Y - 1);
    RecursiveFill(X, Y + 1);
  end;

begin
  SourceColor := fImage.Pixel[oX, oY];
  if SourceColor = aColorIndex then Exit;

  if (fSelectRect.Width = 0) or (fSelectRect.Height = 0) then
    LimitRect := Rect(0, 0, fImage.Width, fImage.Height)
  else
    LimitRect := fSelectRect;

  RecursiveFill(oX, oY);
end;

procedure TFPhart.ShowAbout;
begin
  FAboutPhart.SetVersion(VERSION_NUMBER);
  FAboutPhart.ShowModal;
end;

procedure TFPhart.ShowSheetManager;
begin
  FSheetManager.Show;
end;

procedure TFPhart.DoQuickPalette(aWithTransparency: Boolean);
var
  BaseColor: TColor32;
  BaseIndex: Integer;
  A, R, G, B: Integer;

  VariantIndex: Integer;
  IsAlphaVariant: Boolean;

  i: Integer;
begin
  SuspendSaveUndo;
  try
    BaseColor := fImage.Palette[fPrimaryColorIndex];
    A := AlphaComponent(BaseColor);
    R := RedComponent(BaseColor);
    G := GreenComponent(BaseColor);
    B := BlueComponent(BaseColor);

    IsAlphaVariant := aWithTransparency and (fPrimaryColorIndex mod 8 >= 4);
    if (not IsAlphaVariant) or (A = 255) then A := 128;

    VariantIndex := 5 - (fPrimaryColorIndex mod 4);
    R := R * 5 div VariantIndex;
    G := G * 5 div VariantIndex;
    B := B * 5 div VariantIndex;

    if aWithTransparency then
      BaseIndex := (fPrimaryColorIndex div 8) * 8
    else
      BaseIndex := (fPrimaryColorIndex div 4) * 4;

    for i := 0 to 3 do
      fImage.Palette[BaseIndex + i] := Color32(Min(R * (5 - i) div 5, 255),
                                               Min(G * (5 - i) div 5, 255),
                                               Min(B * (5 - i) div 5, 255),
                                               255);

    if aWithTransparency then
      for i := 0 to 3 do
        fImage.Palette[BaseIndex + i + 4] := (fImage.Palette[BaseIndex + i] and $FFFFFF) or (A shl 24);

    if BaseIndex = 0 then
      fImage.Palette[0] := fImage.Palette[0] and $00FFFFFF;
  finally
    ResumeSaveUndo(false);
  end;

  GeneratePaletteGraphic;
  DrawSelectedColors;
  RedrawImage;
end;

procedure TFPhart.ResetPaletteToDefault;
begin
  fImage.SetPaletteToDefault;
  GeneratePaletteGraphic;
  DrawSelectedColors;
  RedrawImage;
end;

procedure TFPhart.LoadDirect(aStream: TStream);
begin
  fImage.LoadFromStream(aStream);

  fFilename := '';
  fIsSaved := true;

  Caption := 'Phart';

  ClearUndo;
  ClearSelection;
  GeneratePaletteGraphic;
  DrawSelectedColors;
  RedrawImage(true);
end;

procedure TFPhart.SaveDirect(aStream: TStream);
begin
  fImage.SaveToStream(aStream);
  fFilename := '';
  fIsSaved := true;

  Caption := 'Phart';
end;

procedure TFPhart.LoadSampleDirect(aSrc: TBitmap32; aClonePositions: Integer);
var
  i: Integer;
begin
  fSampleImage.Assign(aSrc);

  if fSampleOpacity = 0 then
  begin
    fSampleOpacity := 4;
    if fSampleLayer.Index > fImageLayer.Index then
      fSampleLayer.Index := fImageLayer.Index;
  end;
  fSampleZoom := 1;
  fSampleOffsetX := -aSrc.Width / 3 * fZoom;
  fSampleOffsetY := -aSrc.Height / 3 * fZoom;

  for i := 0 to 8 do
    fSampleClonePositions[i mod 3, i div 3] := (aClonePositions and (1 shl i)) <> 0;

  GenerateSample;
  PositionLayers;
end;

end.

