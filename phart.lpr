program phart;

// Copyright (c) 2020 Namida Verasche
// Phart source code is made available under the MIT Licence
// See readme.txt for more information

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, phart_main, palimage, phart_sizeform, phart_colorform, phart_about,
  phart_sheetform, phart_sheetsizeform, phart_sheethelp
  { you can add units after this };

{$R *.res}


begin
  {$IF DECLARED(GlobalSkipIfNoLeaks)}
  // don't show empty heaptrc output
  GlobalSkipIfNoLeaks := True;
  {$ENDIF}

  RequireDerivedFormResource:=True;
  Application.Title:='Phart';
  Application.Scaled:=True;
  Application.Initialize;
  Application.CreateForm(TFPhart, FPhart);
  Application.CreateForm(TFImageSize, FImageSize);
  Application.CreateForm(TFSelectColor, FSelectColor);
  Application.CreateForm(TFAboutPhart, FAboutPhart);
  Application.CreateForm(TFSheetManager, FSheetManager);
  Application.CreateForm(TFSheetSize, FSheetSize);
  Application.CreateForm(TFSheetManagerHelp, FSheetManagerHelp);
  Application.Run;
end.

