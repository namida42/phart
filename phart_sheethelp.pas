unit phart_sheethelp;

// Copyright (c) 2020 Namida Verasche
// Phart source code is made available under the MIT Licence
// See readme.txt for more information

{$mode objfpc}{$H+}

interface

uses
  GR32, GR32_Png, GR32_Resamplers,
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, GR32_Image;

type

  { TFSheetManagerHelp }

  TFSheetManagerHelp = class(TForm)
    btnOK: TButton;
    imgHelp: TImage32;
    imgNew: TImage32;
    imgToEdit: TImage32;
    imgOpen: TImage32;
    imgFromEdit: TImage32;
    imgResize: TImage32;
    imgSelf: TImage32;
    imgSave: TImage32;
    imgToSample: TImage32;
    Label1: TLabel;
    lblNew: TLabel;
    lblToEdit: TLabel;
    lblFromEdit: TLabel;
    lblToSample: TLabel;
    lblSelf: TLabel;
    lblResize: TLabel;
    lblOpen: TLabel;
    lblHelp: TLabel;
    lblSave: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    procedure PrepareToolButtons;
  public
  end;

var
  FSheetManagerHelp: TFSheetManagerHelp;

implementation

{$R *.lfm}

procedure TFSheetManagerHelp.FormCreate(Sender: TObject);
begin
  PrepareToolButtons;
end;

procedure TFSheetManagerHelp.PrepareToolButtons;
var
  SrcBitmap: TBitmap32;
  R: TResourceStream;

  procedure SetupButton(aTarget: TImage32; aImageIndex: Integer);
  var
    BMP: TBitmap32;
    SrcRect: TRect;
  begin
    BMP := aTarget.Bitmap;
    BMP.SetSize(32, 32);

    SrcBitmap.DrawTo(BMP, 0, 0, Rect(0, 0, 32, 32));

    SrcRect := Rect((aImageIndex mod 10) * 32, (aImageIndex div 10) * 32, 0, 0);
    SrcRect.Right := SrcRect.Left + 32;
    SrcRect.Bottom := SrcRect.Top + 32;

    SrcBitmap.DrawTo(BMP, 0, 0, SrcRect);

    if Screen.PixelsPerInch mod 96 <> 0 then
      TLinearResampler.Create(BMP);
  end;
begin
  SrcBitmap := TBitmap32.Create;
  try
    R := TResourceStream.Create(HInstance, 'UI', 'BUTTONS');
    try
      LoadBitmap32FromPng(SrcBitmap, R);
    finally
      R.Free;
    end;

    SrcBitmap.DrawMode := dmBlend;

    SetupButton(imgNew, 2);
    SetupButton(imgOpen, 3);
    SetupButton(imgSave, 4);

    SetupButton(imgToEdit, 26);
    SetupButton(imgFromEdit, 27);
    SetupButton(imgToSample, 28);

    SetupButton(imgResize, 13);
    SetupButton(imgSelf, 29);
    SetupButton(imgHelp, 22);
  finally
    SrcBitmap.Free;
  end;
end;

end.

